package com.shevtsovaa.service;

import com.shevtsovaa.models.consignments.componentKind.ComponentKind;
import com.shevtsovaa.models.consignments.componentKind.ComponentKindDTO;
import com.shevtsovaa.models.consignments.componentKind.ComponentKindRepo;
import com.shevtsovaa.models.consignments.componentKind.ComponentKindServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.Mockito.when;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Andrey Shevtsov on 19.06.2024.
 * @project warehouse-balance-webapp
 */


@ExtendWith(MockitoExtension.class)
public class ComponentKindServiceTest {

    @Mock
    private ComponentKindRepo componentKindRepo;

    @InjectMocks
    private ComponentKindServiceImpl componentKindService;

    @Test
    public void ComponentKindService_CreateComponentKind_ReturnComponentKindDTO() {
        ComponentKind componentKind = ComponentKind.builder()
                .name("electronics")
                .build();
        ComponentKindDTO componentKindDTO = ComponentKindDTO.builder()
                .name("something else").build();
        when(componentKindRepo.save(Mockito.any(ComponentKind.class))).thenReturn(componentKind);

        ComponentKindDTO savedComponentKind = componentKindService.createComponentKind(componentKindDTO);

        assertThat(savedComponentKind).isNotNull();
    }

    @Test
    public void ComponentKindService_GetComponentKindById_ReturnComponentKindDTO() {

        ComponentKind componentKind = ComponentKind.builder()
                .name("electronics")
                .build();

        when(componentKindRepo.findById(1L)).thenReturn(Optional.ofNullable(componentKind));
        ComponentKindDTO savedComponentKind = componentKindService.findComponentKindById(1L);
        assertThat(savedComponentKind).isNotNull();
    }

    @Test
    public void ComponentKindService_UpdateComponentKind_ReturnComponentKindDTO() {

        ComponentKind componentKind = ComponentKind.builder()
                .name("electronics")
                .build();

        ComponentKindDTO componentKindDTO = ComponentKindDTO.builder()
                .name("something else").build();
        when(componentKindRepo.save(Mockito.any(ComponentKind.class))).thenReturn(componentKind);
        ComponentKindDTO savedComponentKind = componentKindService.updateComponentKind(componentKindDTO);

        assertThat(savedComponentKind).isNotNull();
    }

    @Test
    public void ComponentKindService_DeleteComponentKindById_ReturnComponentKindDTO() {

        ComponentKind componentKind = ComponentKind.builder()
                .name("electronics")
                .build();

        when(componentKindRepo.findById(1L)).thenReturn(Optional.ofNullable(componentKind));
        ComponentKindDTO savedComponentKind = componentKindService.findComponentKindById(1L);
        assertAll(() -> componentKindService.deleteComponentKind(1L));
    }


}
