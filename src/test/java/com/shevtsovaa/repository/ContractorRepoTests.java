package com.shevtsovaa.repository;

import com.shevtsovaa.models.ContractorType;
import com.shevtsovaa.models.contractors.address.Address;
import com.shevtsovaa.models.contractors.contractor.Contractor;
import com.shevtsovaa.models.contractors.contractor.ContractorRepo;
import com.shevtsovaa.models.contractors.persons.Person;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;
import static org.assertj.core.api.Assertions.assertThat;


/**
 * @author Andrey Shevtsov on 13.06.2024.
 * @project warehouse-balance-webapp
 */

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Rollback(value = false)
public class ContractorRepoTests {

    @Autowired
    private ContractorRepo contractorRepo;

    @Test
    public void testAddContractor() {
        Address address = new Address();
        address.setCountry("USA");
        address.setCity("Chicago");
        address.setStreet("Boulevard of broken dreams");
        address.setBuilding("250");
        address.setPostalCode("555 555");

        Person person = new Person();
        person.setFirstName("John");
        person.setLastName("Lennon");
        person.setPosition("chairman");
        person.setPhoneNumber("40-233-6699-987");

        Contractor contractor = new Contractor();
        contractor.setName("Chicago metal inc.");
        contractor.setType(ContractorType.SUPPLIER);
        address.setContractor(contractor);
        person.setContractor(contractor);

        Contractor savedContractor = contractorRepo.save(contractor);
        assertThat(savedContractor).isNotNull();
        contractorRepo.delete(savedContractor);
    }

}
