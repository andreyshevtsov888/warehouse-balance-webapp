package com.shevtsovaa.repository;

import com.shevtsovaa.models.consignments.componentKind.ComponentKind;
import com.shevtsovaa.models.consignments.componentKind.ComponentKindRepo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Andrey Shevtsov on 12.06.2024.
 * @project warehouse-balance-webapp
 */


@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Rollback(value = false)
public class ComponentKindRepoTest {

    @Autowired
    private ComponentKindRepo componentKindRepo;

    @Test
    public void testCreateNewComponentKind() {

        ComponentKind savedComponentKind = componentKindRepo.save(new ComponentKind("electronics"));
        assertThat(savedComponentKind).isNotNull();
        assertThat(savedComponentKind.getComponentKindId()).isGreaterThan(0);
        componentKindRepo.delete(savedComponentKind);
    }

    @Test
    public void ComponentKindRepository_GetAll_ReturnMoreThanOne() {
        List<ComponentKind> componentKindsListBefore = componentKindRepo.findAll();
        int sizeBefore = componentKindsListBefore.size();
        ComponentKind componentKind = ComponentKind.builder()
                .name("electronics")
                .build();
        ComponentKind componentKind2 = ComponentKind.builder()
                .name("electronics")
                .build();

        componentKindRepo.save(componentKind);
        componentKindRepo.save(componentKind2);
        List<ComponentKind> componentKindsListAfter = componentKindRepo.findAll();
        int sizeAfter = componentKindsListAfter.size();
        assertThat(componentKindsListAfter).isNotNull();
        assertThat(componentKindsListAfter.size() - componentKindsListBefore.size()).isEqualTo(2);
    }

    @Test
    public void ComponentKindRepository_FinById_ComponentKindRepository() {
        ComponentKind componentKind = ComponentKind.builder()
                .name("electronics")
                .build();
        componentKindRepo.save(componentKind);

        ComponentKind componentKind1 = componentKindRepo.findById(componentKind.getComponentKindId()).get();
        assertThat(componentKind).isNotNull();

    }

    @Test
    public void ComponentKindRepository_FinByName_ReturnComponentKindRepositoryNotNull() {
        ComponentKind componentKind = ComponentKind.builder()
                .name("electronics")
                .build();
        componentKindRepo.save(componentKind);

        ComponentKind componentKindSaved= componentKindRepo.findById(componentKind.getComponentKindId()).get();
        componentKindSaved.setName("Electronics");
        ComponentKind updateComponentKind = componentKindRepo.save(componentKindSaved);
        assertThat(updateComponentKind.getName()).isNotNull();
    }

    @Test
    public void ComponentKindRepository_ComponentKindDelete_ReturnComponentKindRepositoryIsEmpty() {
        ComponentKind componentKind = ComponentKind.builder()
                .name("electronics")
                .build();
        componentKindRepo.save(componentKind);

        componentKindRepo.deleteById(componentKind.getComponentKindId());
        Optional<ComponentKind> componentKindReturn = componentKindRepo.findById(componentKind.getComponentKindId());

        assertThat(componentKindReturn).isEmpty();
    }

}

