package com.shevtsovaa.repository;

import com.shevtsovaa.models.warehouse.maesureUnit.MeasureUnit;
import com.shevtsovaa.models.warehouse.maesureUnit.MeasureUnitRepo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Andrey Shevtsov on 12.06.2024.
 * @project warehouse-balance-webapp
 */

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Rollback(value = false)

public class MeasureUnitRepoTests {

    @Autowired
    private MeasureUnitRepo measureUnitRepo;

    @Test
    public void testCreateNewCMeasureUnit() {

        MeasureUnit savedMeasureUnit = measureUnitRepo.save(new MeasureUnit("test"));
        assertThat(savedMeasureUnit.getMeasureUnitId()).isGreaterThan(0);
        measureUnitRepo.delete(savedMeasureUnit);
    }
}
