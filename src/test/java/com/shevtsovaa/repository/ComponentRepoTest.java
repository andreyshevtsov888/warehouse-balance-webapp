package com.shevtsovaa.repository;

import com.shevtsovaa.models.consignments.component.Component;
import com.shevtsovaa.models.consignments.component.ComponentRepo;
import com.shevtsovaa.models.consignments.componentKind.ComponentKind;
import com.shevtsovaa.models.warehouse.maesureUnit.MeasureUnit;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Andrey Shevtsov on 12.06.2024.
 * @project warehouse-balance-webapp
 */


@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Rollback(value = false)
public class ComponentRepoTest {

    @Autowired
    private ComponentRepo componentRepo;

    @Test
    public void testCreateNewComponent() {

        Component savedComponent = componentRepo.save(new Component(12, "Diod 111"));
        assertThat(savedComponent).isNotNull();
        assertThat(savedComponent.getComponentId()).isGreaterThan(0);
        componentRepo.delete(savedComponent);
    }

    @Test
    public void ComponentRepository_GetAll_ReturnMoreThanOne() {
        List<Component> componentsListBefore = componentRepo.findAll();
        int sizeBefore = componentsListBefore.size();
        Component component = Component.builder()
                .name("Diod 111")
                .measureUnit(new MeasureUnit("kilo"))
                .componentKind(new ComponentKind("Electronics"))
                .minQuantity(40F)
                .build();
        Component component2 = Component.builder()
                .name("Diod 222")
                .measureUnit(new MeasureUnit("pcs"))
                .componentKind(new ComponentKind("Electronics"))
                .minQuantity(50F)
                .build();

        componentRepo.save(component);
        componentRepo.save(component2);
        List<Component> componentsListAfter = componentRepo.findAll();
        int sizeAfter = componentsListAfter.size();
        assertThat(componentsListAfter).isNotNull();
        assertThat(componentsListAfter.size() - componentsListBefore.size()).isEqualTo(2);
    }

    @Test
    public void ComponentRepository_FinById_ComponentRepository() {
        Component component = Component.builder()
                .name("electronics")
                .build();
        componentRepo.save(component);

        Component component1 = componentRepo.findById(component.getComponentId()).get();
        assertThat(component).isNotNull();

    }

    @Test
    public void ComponentRepository_FinByName_ReturnComponentRepositoryNotNull() {
        Component component = Component.builder()
                .name("electronics")
                .description("something")
                .minQuantity(2f)
                .componentKind(new ComponentKind())
                .measureUnit(new MeasureUnit())
                .build();
        componentRepo.save(component);

        Component componentSaved= componentRepo.findById(component.getComponentId()).get();
        componentSaved.setName("Electronics");
        Component updateComponent = componentRepo.save(componentSaved);
        assertThat(updateComponent.getName()).isNotNull();
    }

    @Test
    public void ComponentRepository_ComponentDelete_ReturnComponentRepositoryIsEmpty() {
        Component component = Component.builder()
                .name("electronics")
                .description("something")
                .minQuantity(2f)
                .componentKind(new ComponentKind())
                .measureUnit(new MeasureUnit())
                .build();
        componentRepo.save(component);

        componentRepo.deleteById(component.getComponentId());
        Optional<Component> componentReturn = componentRepo.findById(component.getComponentId());

        assertThat(componentReturn).isEmpty();
    }

}

