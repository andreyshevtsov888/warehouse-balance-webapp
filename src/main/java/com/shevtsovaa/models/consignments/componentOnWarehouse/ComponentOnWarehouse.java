package com.shevtsovaa.models.consignments.componentOnWarehouse;

import com.shevtsovaa.models.consignments.consignment.Consignment;
import com.shevtsovaa.models.consignments.component.Component;
import jakarta.persistence.*;
import lombok.*;

import java.math.BigDecimal;

/**
 * @author Andrey Shevtsov on 22.05.2024.
 * @project warehouse-balance-webapp
 */

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Builder
@Entity
@Table(name = "components_on_warehouse")
@AllArgsConstructor
public class ComponentOnWarehouse {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Long componentOnWarehouseId;
    @Column(nullable = false)
    private BigDecimal quantity;
    @Column(nullable = false)
    private BigDecimal price;
    @Column(columnDefinition="BOOLEAN DEFAULT false")
    private Boolean isSendToWarehouse;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "component_id")
    @ToString.Exclude
    private Component component;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "consignment_id")
    @ToString.Exclude
    private Consignment consignment;


}
