package com.shevtsovaa.models.consignments.componentOnWarehouse;

import com.shevtsovaa.models.consignments.component.ComponentDTO;
import com.shevtsovaa.models.consignments.component.ComponentService;
import com.shevtsovaa.models.consignments.consignment.ConsignmentDTO;
import com.shevtsovaa.models.consignments.consignment.ConsignmentService;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Controller
@Slf4j
public class ComponentOnWarehouseController {

    private final ComponentOnWarehouseService componentOnWarehouseService;
    private final ComponentService componentService;
    private final ConsignmentService consignmentService;

    @Autowired
    public ComponentOnWarehouseController(ComponentOnWarehouseService componentOnWarehouseService, ComponentService componentService, ConsignmentService consignmentService) {
        this.componentOnWarehouseService = componentOnWarehouseService;
        this.componentService = componentService;
        this.consignmentService = consignmentService;
    }

    @GetMapping("/componentsOnWarehouse")
    public String listComponentOnWarehouses(Model model) {
        List<ComponentOnWarehouseDTO> componentOnWarehousesList = componentOnWarehouseService.findAllComponentOnWarehouses();
        model.addAttribute("componentOnWarehousesList", componentOnWarehousesList);
        return "componentOnWarehouse/component-on-warehouse-list";
    }

    @GetMapping("componentsOnWarehouse/{consignmentId}/new")
    public String createComponentOnWarehouseForm(@PathVariable("consignmentId") Long consignmentId, Model model) {
        ComponentOnWarehouse componentOnWarehouse = new ComponentOnWarehouse();
        model.addAttribute("consignmentId", consignmentId);
        model.addAttribute("componentOnWarehouse", componentOnWarehouse);
        List<ComponentDTO> componentsList = componentService.findAllComponents();
        model.addAttribute("componentsList", componentsList);
        return "componentOnWarehouse/component-on-warehouse-create";
    }

    @PostMapping("componentsOnWarehouse/{consignmentId}")
    public String createComponentOnWarehouse(@PathVariable("consignmentId") Long consignmentId,
                                             @ModelAttribute("consignment") ComponentOnWarehouseDTO componentOnWarehouseDTO, Model model) {
        model.addAttribute("consignmentId", consignmentId);
        model.addAttribute("componentOnWarehouse", componentOnWarehouseDTO);
        componentOnWarehouseDTO.setIsSendToWarehouse(false);
        componentOnWarehouseService.createComponentOnWarehouse(consignmentId, componentOnWarehouseDTO);
        return "redirect:/consignment/{consignmentId}";
    }

    @GetMapping("componentsOnWarehouse/{componentOnWarehouseId}/edit")
    public String editComponentOnWarehouseForm(@PathVariable("componentOnWarehouseId") Long componentOnWarehouseId, Model model) {
        ComponentOnWarehouseDTO componentOnWarehouseDTO = componentOnWarehouseService.findComponentOnWarehouseById(componentOnWarehouseId);
        model.addAttribute("componentOnWarehouse", componentOnWarehouseDTO);
        List<ComponentDTO> componentsList = componentService.findAllComponents();
        model.addAttribute("componentsList", componentsList);
        List<ConsignmentDTO> consignmentsList = consignmentService.findAllConsignments();
        model.addAttribute("consignmentsList", consignmentsList);
        return "componentOnWarehouse/component-on-warehouse-edit";
    }

    @PostMapping("componentsOnWarehouse/{componentOnWarehouseId}/edit")
    public String updateComponentOnWarehouse(@Valid @PathVariable("componentOnWarehouseId") Long componentOnWarehouseId,
                                             @ModelAttribute("component") ComponentOnWarehouseDTO componentOnWarehouseDTO,
                                             BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute("componentOnWarehouseDTO", componentOnWarehouseDTO);
            return "componentOnWarehouse/component-on-warehouse-edit";
        }
        ComponentOnWarehouseDTO componentOnWarehouseDTO1 = componentOnWarehouseService.findComponentOnWarehouseById(componentOnWarehouseId);
        componentOnWarehouseDTO.setIsSendToWarehouse(componentOnWarehouseDTO1.getIsSendToWarehouse());
        componentOnWarehouseDTO.setComponentOnWarehouseId(componentOnWarehouseId);
        componentOnWarehouseDTO.setConsignment(componentOnWarehouseDTO1.getConsignment());
        componentOnWarehouseService.updateComponentOnWarehouse(componentOnWarehouseDTO);
        return "redirect:/componentsOnWarehouse";
    }

    @GetMapping("componentsOnWarehouse/{componentOnWarehouseId}/delete")
    public String deleteComponentOnWarehouse(@PathVariable("componentOnWarehouseId") Long componentOnWarehouseId) {
        componentOnWarehouseService.deleteComponentOnWarehouse(componentOnWarehouseId);
        return "redirect:/componentsOnWarehouse";
    }

}
