package com.shevtsovaa.models.consignments.componentOnWarehouse;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Service
public interface ComponentOnWarehouseService {

    ComponentOnWarehouseDTO findComponentOnWarehouseById(Long componentOnWarehouseId);
    List<ComponentOnWarehouseDTO> findAllComponentOnWarehouses();
    List<ComponentOnWarehouseDTO> findAllComponentOnWarehousesByConsignment(Long consignmentId);
    void updateComponentOnWarehouse(ComponentOnWarehouseDTO componentOnWarehouseDTO);
    void deleteComponentOnWarehouse(Long componentOnWarehouseId);
    void createComponentOnWarehouse(Long consignmentId, ComponentOnWarehouseDTO componentOnWarehouseDTO);
    void addTotalSumToConsignment(Long consignmentId, ComponentOnWarehouse componentOnWarehouse);
    void subtractTotalSumFromConsignment(Long componentOnWarehouseId);
    void updateTotalSumInConsignment(BigDecimal before, BigDecimal after, Long consignmentId);

}
