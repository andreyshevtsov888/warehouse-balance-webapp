package com.shevtsovaa.models.consignments.componentOnWarehouse;

import com.shevtsovaa.models.consignments.component.Component;
import com.shevtsovaa.models.consignments.consignment.Consignment;
import lombok.*;

import java.math.BigDecimal;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Data
@Builder
public class ComponentOnWarehouseDTO {

    private Long componentOnWarehouseId;
    private BigDecimal quantity;
    private BigDecimal price;
    private Boolean isSendToWarehouse;
    private Component component;
    private Consignment consignment;

}
