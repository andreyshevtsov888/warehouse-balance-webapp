package com.shevtsovaa.models.consignments.componentOnWarehouse;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Repository
public interface ComponentOnWarehouseRepo extends JpaRepository<ComponentOnWarehouse, Long> {

    @Query(value = "FROM ComponentOnWarehouse WHERE consignment.consignmentId = :query")
    List<ComponentOnWarehouse> findComponentOnWarehouseByConsignment(Long query);

}


