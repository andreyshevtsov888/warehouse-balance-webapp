package com.shevtsovaa.models.consignments.componentOnWarehouse;

import com.shevtsovaa.exceptions.ApiRequestException;
import com.shevtsovaa.exceptions.ItemNotFoundException;
import com.shevtsovaa.models.consignments.consignment.Consignment;
import com.shevtsovaa.models.consignments.consignment.ConsignmentRepo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import static com.shevtsovaa.models.consignments.componentOnWarehouse.ComponentOnWarehouseMapper.*;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Service
@Slf4j
public class ComponentOnWarehouseServiceImpl implements ComponentOnWarehouseService {


    private final ComponentOnWarehouseRepo componentOnWarehouseRepo;
    private final ConsignmentRepo consignmentRepo;

    @Autowired
    public ComponentOnWarehouseServiceImpl(ComponentOnWarehouseRepo componentOnWarehouseRepo, ConsignmentRepo consignmentRepo) {
        this.componentOnWarehouseRepo = componentOnWarehouseRepo;
        this.consignmentRepo = consignmentRepo;
    }

    @Override
    public ComponentOnWarehouseDTO findComponentOnWarehouseById(Long ComponentOnWarehouseId) {
        ComponentOnWarehouse componentOnWarehouse = componentOnWarehouseRepo.findById(ComponentOnWarehouseId).orElseThrow(
                () -> new ApiRequestException("The component on warehouse could not be found by ID."));
        log.info("Component on warehouse was find by id successfully.");
        return mapToComponentOnWarehouseDTO(componentOnWarehouse);
    }

    @Override
    public List<ComponentOnWarehouseDTO> findAllComponentOnWarehouses() {
        List<ComponentOnWarehouse> componentOnWarehouses = componentOnWarehouseRepo.findAll();
        log.info("Component on warehouse list was gotten from base");
        return componentOnWarehouses.stream().map(ComponentOnWarehouseMapper::mapToComponentOnWarehouseDTO).collect(Collectors.toList());
    }

    @Override
    public List<ComponentOnWarehouseDTO> findAllComponentOnWarehousesByConsignment(Long consignmentId) {
        List<ComponentOnWarehouse> componentOnWarehousesByConsignmentList = componentOnWarehouseRepo.findComponentOnWarehouseByConsignment(consignmentId);
        log.info("Component on warehouse by consignment list was gotten from base");
        return componentOnWarehousesByConsignmentList.stream().map(ComponentOnWarehouseMapper::mapToComponentOnWarehouseDTO).collect(Collectors.toList());
    }

    @Override
    public void createComponentOnWarehouse(Long consignmentId, ComponentOnWarehouseDTO componentOnWarehouseDTO) {
        Consignment consignment = consignmentRepo.findById(consignmentId).orElseThrow(
                () -> new ApiRequestException("The consignment could not be found by ID."));
        ComponentOnWarehouse componentOnWarehouse = mapToComponentOnWarehouse(componentOnWarehouseDTO);
        addTotalSumToConsignment(consignmentId, componentOnWarehouse);
        componentOnWarehouse.setConsignment(consignment);
        componentOnWarehouseRepo.save(componentOnWarehouse);
        log.info("Component on warehouse was saved successfully.");

    }

    @Override
    public void updateComponentOnWarehouse(ComponentOnWarehouseDTO componentOnWarehouseDTO) {
        ComponentOnWarehouse componentOnWarehouse = mapToComponentOnWarehouse(componentOnWarehouseDTO);
        if (componentOnWarehouse.getIsSendToWarehouse())
            throw new ApiRequestException("You can't edit this item already");
        ComponentOnWarehouse cowBefore = componentOnWarehouseRepo.findById(componentOnWarehouse.getComponentOnWarehouseId()).orElseThrow(
                () -> new ApiRequestException("The component on warehouse could not be found by ID."));
        BigDecimal sumBefore = cowBefore.getPrice().multiply(cowBefore.getQuantity());
        BigDecimal sumAfter = componentOnWarehouse.getPrice().multiply(componentOnWarehouse.getQuantity());
        Long consignmentId = componentOnWarehouse.getConsignment().getConsignmentId();
        updateTotalSumInConsignment(sumBefore, sumAfter, consignmentId);
        componentOnWarehouseRepo.save(componentOnWarehouse);
        log.info("Component on warehouse was updated successfully.");

    }

    @Override
    public void deleteComponentOnWarehouse(Long componentOnWarehouseId) {
        if (componentOnWarehouseRepo.findById(componentOnWarehouseId).orElseThrow(
                () -> new ApiRequestException("The component on warehouse could not be found by ID.")).getIsSendToWarehouse())
            throw new ApiRequestException("You can't delete this item already");
        componentOnWarehouseRepo.findById(componentOnWarehouseId)
                .orElseThrow(() -> new ItemNotFoundException("The component on warehouse could not be deleted."));
        subtractTotalSumFromConsignment(componentOnWarehouseId);
        try {
            componentOnWarehouseRepo.deleteById(componentOnWarehouseId);
        } catch (RuntimeException e) {
            throw new ApiRequestException("You can't delete this item!");
        }
        log.info("Component on warehouse was deleted successfully.");
    }

    @Override
    public void addTotalSumToConsignment(Long consignmentId, ComponentOnWarehouse componentOnWarehouse) {
        Consignment consignment = consignmentRepo.findById(consignmentId)
                .orElseThrow(() -> new ItemNotFoundException("Could not find this consignment."));
        consignment.setTotalSum(consignment.getTotalSum().add(componentOnWarehouse.getQuantity().multiply(componentOnWarehouse.getPrice())));
        log.info("New total sum after add was set on :  {}", consignment.getTotalSum());
    }

    @Override
    public void subtractTotalSumFromConsignment(Long componentOnWarehouseId) {
        ComponentOnWarehouse componentOnWarehouse = componentOnWarehouseRepo.findById(componentOnWarehouseId)
                .orElseThrow(() -> new ItemNotFoundException("Could not find this component on warehouse."));
        Consignment consignment = consignmentRepo.findById(componentOnWarehouse.getConsignment().getConsignmentId())
                .orElseThrow(() -> new ItemNotFoundException("Could not find this consignment."));
        consignment.setTotalSum(consignment.getTotalSum().subtract(componentOnWarehouse.getQuantity().multiply(componentOnWarehouse.getPrice())));
        log.info("New total sum after subtract was set on :  {}", consignment.getTotalSum());
    }

    @Override
    public void updateTotalSumInConsignment(BigDecimal before, BigDecimal after, Long consignmentId) {
        Consignment consignment = consignmentRepo.findById(consignmentId)
                .orElseThrow(() -> new ItemNotFoundException("Could not find this consignment."));
        BigDecimal gap = after.subtract(before);
        consignment.setTotalSum(consignment.getTotalSum().add(gap));
        log.info("New total sum after update was set on :  {}", consignment.getTotalSum());
    }

}
