package com.shevtsovaa.models.consignments.componentOnWarehouse;

import com.shevtsovaa.models.warehouse.warehouseBalance.WarehouseBalance;

/**
 * @author Andrey Shevtsov on 12.06.2024.
 * @project warehouse-balance-webapp
 */
public class ComponentOnWarehouseMapper {

    public static ComponentOnWarehouse mapToComponentOnWarehouse(ComponentOnWarehouseDTO componentOnWarehouseDTO) {
        return ComponentOnWarehouse.builder()
                .componentOnWarehouseId(componentOnWarehouseDTO.getComponentOnWarehouseId())
                .quantity(componentOnWarehouseDTO.getQuantity())
                .price(componentOnWarehouseDTO.getPrice())
                .isSendToWarehouse(componentOnWarehouseDTO.getIsSendToWarehouse())
                .consignment(componentOnWarehouseDTO.getConsignment())
                .component(componentOnWarehouseDTO.getComponent())
                .build();
    }


    public static ComponentOnWarehouseDTO mapToComponentOnWarehouseDTO(ComponentOnWarehouse componentOnWarehouse) {
        return ComponentOnWarehouseDTO.builder()
                .componentOnWarehouseId(componentOnWarehouse.getComponentOnWarehouseId())
                .quantity(componentOnWarehouse.getQuantity())
                .price(componentOnWarehouse.getPrice())
                .isSendToWarehouse(componentOnWarehouse.getIsSendToWarehouse())
                .consignment(componentOnWarehouse.getConsignment())
                .component(componentOnWarehouse.getComponent())
                .build();
    }

    public static WarehouseBalance mapToWarehouseBalanceFromCOW(ComponentOnWarehouse componentOnWarehouse) {
        return WarehouseBalance.builder()
                .warehouseBalanceId(componentOnWarehouse.getComponentOnWarehouseId())
                .quantity(componentOnWarehouse.getQuantity())
                .price(componentOnWarehouse.getPrice())
                .consignment(componentOnWarehouse.getConsignment())
                .component(componentOnWarehouse.getComponent())
                .build();
    }

}
