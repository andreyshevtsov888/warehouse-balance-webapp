package com.shevtsovaa.models.consignments.component;

import com.shevtsovaa.models.deliveryNotes.product.Product;
import com.shevtsovaa.models.warehouse.maesureUnit.MeasureUnit;
import com.shevtsovaa.models.consignments.componentKind.ComponentKind;

import jakarta.validation.constraints.NotEmpty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Data
@Builder
public class ComponentDTO {

    private long componentId;
    @NotEmpty(message = "Component name should not be empty.")
    private String name;
    private String description;
    private Float minQuantity;
    private ComponentKind componentKind;
    private MeasureUnit measureUnit;
    private List<Product> productsList;
}

