package com.shevtsovaa.models.consignments.component;

import com.shevtsovaa.exceptions.ApiRequestException;
import com.shevtsovaa.exceptions.ItemNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static com.shevtsovaa.models.consignments.component.ComponentMapper.mapToComponent;
import static com.shevtsovaa.models.consignments.component.ComponentMapper.mapToComponentDTO;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Service
@Slf4j
public class ComponentServiceImpl implements ComponentService {

    @Autowired
    private ComponentRepo componentRepo;

    @Override
    public ComponentDTO findComponentById(Long componentId) {
        Component component = componentRepo.findById(componentId).orElseThrow(
                () -> new ApiRequestException("The component could not be found by ID."));
        log.info("Component was find by id successfully.");
        return mapToComponentDTO(component);
    }

    @Override
    public List<ComponentDTO> findAllComponents() {
        List<Component> componentsList = componentRepo.findAll();
        log.info("Component's list was gotten from base");
        return componentsList.stream().map(ComponentMapper::mapToComponentDTO).collect(Collectors.toList());
    }

    @Override
    public void saveComponent(ComponentDTO componentDTO) {
        Component component = mapToComponent(componentDTO);
        if (isComponentExistsInList(componentDTO.getName())) {
            log.info("Component wasn't saved.");
            throw new ApiRequestException("Component with that name already exists.");
        } else {
            componentRepo.save(component);
            log.info("Component was saved successfully.");
        }
    }

    @Override
    public void updateComponent(ComponentDTO componentDTO) {
        Component component = mapToComponent(componentDTO);
        log.info("Component was updated successfully.");
        componentRepo.save(component);
    }

    @Override
    public void deleteComponent(Long componentId) {
        componentRepo.findById(componentId).orElseThrow(() -> new ItemNotFoundException("The component could not be deleted."));
        try {
            componentRepo.deleteById(componentId);
        } catch (RuntimeException e) {
            throw new ApiRequestException("You can't delete this item!");
        }
        log.info("Component was deleted successfully.");
    }

    @Override
    public List<ComponentDTO> searchComponents(String query) {
        List<Component> components = componentRepo.searchComponents(query);
        return components.stream().map(ComponentMapper::mapToComponentDTO).toList();
    }

    @Override
    public boolean isComponentExistsInList(String name) {
        return !searchComponents(name).isEmpty();
    }

    @Override
    public List<ComponentDTO> findAllComponentsInProduct(Long productId) {
        List<Component> componentsList = componentRepo.findAll();
        log.info("Components in product list was gotten from base");
        return componentsList.stream().map(ComponentMapper::mapToComponentDTO).collect(Collectors.toList());
    }
}
