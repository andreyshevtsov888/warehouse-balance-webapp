package com.shevtsovaa.models.consignments.component;

import com.shevtsovaa.models.consignments.componentKind.ComponentKindDTO;
import com.shevtsovaa.models.warehouse.maesureUnit.MeasureUnitDTO;
import com.shevtsovaa.models.consignments.componentKind.ComponentKindService;
import com.shevtsovaa.models.warehouse.maesureUnit.MeasureUnitService;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Controller
@Slf4j
public class ComponentController {

    private final ComponentService componentService;
    private final ComponentKindService componentKindService;
    private final MeasureUnitService measureUnitService;

    @Autowired
    public ComponentController(ComponentService componentService, ComponentKindService componentKindService, MeasureUnitService measureUnitService) {
        this.componentService = componentService;
        this.componentKindService = componentKindService;
        this.measureUnitService = measureUnitService;
    }

    @GetMapping("/components")
    public String listComponents(Model model) {
        List<ComponentDTO> componentsList = componentService.findAllComponents();
        model.addAttribute("componentsList", componentsList);
        return "component/component-list";
    }

    @GetMapping("components/new")
    public String createComponentForm(Model model) {
        Component component = new Component();
        model.addAttribute("component", component);
        List<ComponentKindDTO> componentKindsList = componentKindService.findAllComponentKinds();
        model.addAttribute("componentKindsList", componentKindsList);
        List<MeasureUnitDTO> measureUnitsList = measureUnitService.findAllMeasureUnits();
        model.addAttribute("measureUnitsList", measureUnitsList);
        return "component/component-create";
    }

    @PostMapping("components/save")
    public String saveComponent(@Valid @ModelAttribute("component") ComponentDTO componentDTO, BindingResult result, Model model) {
        model.addAttribute("component", componentDTO);
        componentService.saveComponent(componentDTO);
        return "redirect:/success";
    }

    @GetMapping("components/{componentId}/edit")
    public String editComponentForm(@PathVariable("componentId") Long componentId, Model model) {
        ComponentDTO componentDTO = componentService.findComponentById(componentId);
        model.addAttribute("component", componentDTO);
        List<ComponentKindDTO> componentKindsList = componentKindService.findAllComponentKinds();
        model.addAttribute("componentKindsList", componentKindsList);
        List<MeasureUnitDTO> measureUnitsList = measureUnitService.findAllMeasureUnits();
        model.addAttribute("measureUnitsList", measureUnitsList);

        return "component/component-edit";
    }

    @PostMapping("components/{componentId}/edit")
    public String updateComponent(@Valid @PathVariable("componentId") Long componentId,
                                  @ModelAttribute("component") ComponentDTO componentDTO,
                                  BindingResult result) {
        if (result.hasErrors()) {
            return "component/component-edit";
        }
        componentDTO.setComponentId(componentId);
        componentService.updateComponent(componentDTO);
        return "redirect:/components";
    }

    @GetMapping("components/{componentId}/delete")
    public String deleteComponent(@PathVariable("componentId") Long componentId) {
        componentService.deleteComponent(componentId);
        return "redirect:/components";
    }

    @GetMapping("components/search")
    public String searchComponents(@RequestParam(value = "query") String query, Model model) {
        List<ComponentDTO> componentsList = componentService.searchComponents(query);
        model.addAttribute("componentsList", componentsList);
        return "component/component-list";
    }

    @GetMapping("components/{componentId}")
    public String componentDetail(@PathVariable("componentId") Long componentId, Model model) {
        ComponentDTO componentDTO = componentService.findComponentById(componentId);
        model.addAttribute("component", componentDTO);
        return "component/component-detail";
    }

}
