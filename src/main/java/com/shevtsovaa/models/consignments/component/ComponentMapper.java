package com.shevtsovaa.models.consignments.component;

/**
 * @author Andrey Shevtsov on 07.06.2024.
 * @project warehouse-balance-webapp
 */

public class ComponentMapper {

    public static Component mapToComponent(ComponentDTO componentDTO) {
        return Component.builder()
                .componentId(componentDTO.getComponentId())
                .name(componentDTO.getName())
                .description(componentDTO.getDescription())
                .minQuantity(componentDTO.getMinQuantity())
                .componentKind(componentDTO.getComponentKind())
                .measureUnit(componentDTO.getMeasureUnit())
                .build();
    }


    public static ComponentDTO mapToComponentDTO(Component component) {
        return ComponentDTO.builder()
                .componentId(component.getComponentId())
                .name(component.getName())
                .description(component.getDescription())
                .minQuantity(component.getMinQuantity())
                .componentKind(component.getComponentKind())
                .measureUnit(component.getMeasureUnit())
                .build();
    }

}
