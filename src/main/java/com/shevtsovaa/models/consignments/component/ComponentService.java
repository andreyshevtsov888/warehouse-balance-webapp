package com.shevtsovaa.models.consignments.component;

import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Service
public interface ComponentService {

    ComponentDTO findComponentById(Long componentId);
    List<ComponentDTO> findAllComponents();
    void saveComponent(ComponentDTO componentDTO);
    void updateComponent(ComponentDTO componentDTO);
    void deleteComponent(Long componentId);
    List<ComponentDTO> searchComponents(String query);
    boolean isComponentExistsInList(String name);
    List<ComponentDTO> findAllComponentsInProduct(Long productId);
}
