package com.shevtsovaa.models.consignments.component;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Repository
public interface ComponentRepo extends JpaRepository<Component, Long> {

    Optional<Component> findComponentByName(String url);

    @Query(value = "SELECT c FROM Component c WHERE c.name LIKE CONCAT('%', :query, '%')")
    List<Component> searchComponents(String query);

}
