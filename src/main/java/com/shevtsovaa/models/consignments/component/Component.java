package com.shevtsovaa.models.consignments.component;

import com.shevtsovaa.models.consignments.componentKind.ComponentKind;
import com.shevtsovaa.models.warehouse.maesureUnit.MeasureUnit;
import jakarta.persistence.*;
import lombok.*;

/**
 * @author Andrey Shevtsov on 22.05.2024.
 * @project warehouse-balance-webapp
 */

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name = "components")
@Builder
@AllArgsConstructor
public class Component {

    public Component(String name) {
        this.name = name;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "component_id",nullable = false)
    private Long componentId;
    @Column(nullable = false, length = 20, unique = true)
    private String name;
    @Column(length = 100)
    private String description;
    @Column
    private Float minQuantity;

    //good
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "component_kind_id")
    @ToString.Exclude
    private ComponentKind componentKind;
    //good
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "measure_unit_id")
    @ToString.Exclude
    private MeasureUnit measureUnit;



}
