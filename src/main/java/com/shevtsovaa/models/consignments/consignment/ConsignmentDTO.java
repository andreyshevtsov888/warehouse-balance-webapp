package com.shevtsovaa.models.consignments.consignment;

import com.shevtsovaa.models.contractors.contractor.Contractor;
import jakarta.validation.constraints.NotEmpty;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Data
@Builder
public class ConsignmentDTO {

    private Long consignmentId;
    @NotEmpty(message = "Consignment number should not be empty.")
    private String number;
    private Date date;
    private int vatLevel;
    private BigDecimal totalSum;
    private Contractor contractor;

}
