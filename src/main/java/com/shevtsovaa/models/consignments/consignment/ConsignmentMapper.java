package com.shevtsovaa.models.consignments.consignment;

/**
 * @author Andrey Shevtsov on 12.06.2024.
 * @project warehouse-balance-webapp
 */

public class ConsignmentMapper {

    public static Consignment mapToConsignment(ConsignmentDTO consignmentDTO) {
        return Consignment.builder()
                .consignmentId(consignmentDTO.getConsignmentId())
                .number(consignmentDTO.getNumber())
                .date(consignmentDTO.getDate())
                .vatLevel(consignmentDTO.getVatLevel())
                .totalSum(consignmentDTO.getTotalSum())
                .contractor(consignmentDTO.getContractor())
                .build();
    }


    public static ConsignmentDTO mapToConsignmentDTO(Consignment consignment) {
        return ConsignmentDTO.builder()
                .consignmentId(consignment.getConsignmentId())
                .number(consignment.getNumber())
                .date(consignment.getDate())
                .vatLevel(consignment.getVatLevel())
                .totalSum(consignment.getTotalSum())
                .contractor(consignment.getContractor())
                .build();
    }

}
