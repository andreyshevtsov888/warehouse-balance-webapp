package com.shevtsovaa.models.consignments.consignment;

import com.shevtsovaa.models.consignments.component.ComponentDTO;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Service
public interface ConsignmentService {

    ConsignmentDTO findConsignmentById(Long consignmentId);
    List<ConsignmentDTO> findAllConsignments();
    void saveConsignment(ConsignmentDTO consignmentDTO);
    void updateConsignment(ConsignmentDTO consignmentDTO);
    void deleteConsignment(Long consignmentId);
    List<ConsignmentDTO> searchConsignmentsByNumber(String query);
    boolean isConsignmentExistsInList(String name);

}
