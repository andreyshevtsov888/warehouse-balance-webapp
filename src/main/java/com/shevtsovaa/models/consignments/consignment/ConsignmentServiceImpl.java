package com.shevtsovaa.models.consignments.consignment;

import com.shevtsovaa.exceptions.ApiRequestException;
import com.shevtsovaa.exceptions.ItemNotFoundException;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import static com.shevtsovaa.models.consignments.consignment.ConsignmentMapper.mapToConsignment;
import static com.shevtsovaa.models.consignments.consignment.ConsignmentMapper.mapToConsignmentDTO;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Service
@Slf4j
public class ConsignmentServiceImpl implements ConsignmentService {

    @Autowired
    ConsignmentRepo consignmentRepo;

    @Override
    public ConsignmentDTO findConsignmentById(Long consignmentId) {
        Consignment consignment = consignmentRepo.findById(consignmentId).orElseThrow(
                () -> new ApiRequestException("The consignment could not be found by ID."));
        log.info("consignment was find by id successfully.");
        return mapToConsignmentDTO(consignment);
    }

    @Override
    public List<ConsignmentDTO> findAllConsignments() {
        List<Consignment> consignmentsList = consignmentRepo.findAll();
        log.info("Consignment's list was gotten from base");
        return consignmentsList.stream().map(ConsignmentMapper::mapToConsignmentDTO).toList();
    }

    @Override
    public void saveConsignment(ConsignmentDTO consignmentDTO) {
        Consignment consignment = mapToConsignment(consignmentDTO);
        consignment.setTotalSum(BigDecimal.valueOf(0));
        consignmentRepo.save(consignment);
        log.info("Consignment was saved successfully.");
    }


    @Override
    public void updateConsignment(ConsignmentDTO consignmentDTO) {
        Consignment consignment = mapToConsignment(consignmentDTO);
        log.info("Consignment was updated successfully.");
        consignmentRepo.save(consignment);
    }

    @Override
    public void deleteConsignment(Long consignmentId) {
        consignmentRepo.findById(consignmentId).orElseThrow(() -> new ItemNotFoundException("The consignment could not be deleted."));
        try {
            consignmentRepo.deleteById(consignmentId);
        } catch (RuntimeException e) {
            throw new ApiRequestException("You can't delete this item!");
        }
        log.info("Consignment was deleted successfully.");
    }

    @Override
    public List<ConsignmentDTO> searchConsignmentsByNumber(String query) {
        List<Consignment> consignmentsList = consignmentRepo.searchConsignmentsByNumber(query);
        return consignmentsList.stream().map(ConsignmentMapper::mapToConsignmentDTO).collect(Collectors.toList());
    }

    @Override
    public boolean isConsignmentExistsInList(String number) {
        return !searchConsignmentsByNumber(number).isEmpty();
    }

}
