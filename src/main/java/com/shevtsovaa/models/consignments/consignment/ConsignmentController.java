package com.shevtsovaa.models.consignments.consignment;

import com.shevtsovaa.models.consignments.componentOnWarehouse.ComponentOnWarehouseDTO;
import com.shevtsovaa.models.consignments.componentOnWarehouse.ComponentOnWarehouseService;
import com.shevtsovaa.models.contractors.contractor.ContractorDTO;
import com.shevtsovaa.models.contractors.contractor.ContractorService;

import jakarta.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Controller
public class ConsignmentController {

    private final ConsignmentService consignmentService;
    private final ComponentOnWarehouseService componentOnWarehouseService;
    private final ContractorService contractorService;

    public ConsignmentController(ConsignmentService consignmentService, ComponentOnWarehouseService componentOnWarehouseService, ContractorService contractorService) {
        this.consignmentService = consignmentService;
        this.componentOnWarehouseService = componentOnWarehouseService;
        this.contractorService = contractorService;
    }

    @GetMapping("/consignments")
    public String listConsignments(Model model) {
        List<ConsignmentDTO> consignmentsList =consignmentService.findAllConsignments();
        model.addAttribute("consignmentsList", consignmentsList);
        return "consignment/consignment-list";
    }

    @GetMapping("consignments/new")
    public String createConsignmentForm(Model model) {
        Consignment consignment = new Consignment();
        model.addAttribute("consignment", consignment);
        List<ContractorDTO> contractorsList = contractorService.findAllContractors();
        model.addAttribute("contractorsList", contractorsList);
        List<ComponentOnWarehouseDTO> componentsOnWarehouseList = componentOnWarehouseService.findAllComponentOnWarehouses();
        model.addAttribute("componentsOnWarehouseList", componentsOnWarehouseList);

        return "consignment/consignment-create";
    }

    @PostMapping("consignments/save")
    public String saveConsignment(@Valid @ModelAttribute("consignment") ConsignmentDTO consignmentDTO, Model model) {
        model.addAttribute("consignment", consignmentDTO);
        consignmentDTO.setTotalSum(BigDecimal.valueOf(0));
        consignmentService.saveConsignment(consignmentDTO);
        return "redirect:/success";
    }

    @GetMapping("consignments/{consignmentId}/edit")
    public String editConsignmentForm(@PathVariable("consignmentId") Long consignmentId, Model model) {
        ConsignmentDTO consignmentDTO = consignmentService.findConsignmentById(consignmentId);
        model.addAttribute("consignment", consignmentDTO);
        List<ContractorDTO> contractorsList = contractorService.findAllContractors();
        model.addAttribute("contractorsList", contractorsList);
        List<ComponentOnWarehouseDTO> componentsOnWarehouseList = componentOnWarehouseService.findAllComponentOnWarehouses();
        model.addAttribute("componentsOnWarehouseList", componentsOnWarehouseList);
        return "consignment/consignment-edit";
    }

    @PostMapping("consignments/{consignmentId}/edit")
    public String updateConsignment(@Valid @PathVariable("consignmentId") Long consignmentId,
                                  @ModelAttribute("consignment") ConsignmentDTO consignmentDTO,
                                  BindingResult result) {
        if (result.hasErrors()) {
            return "consignment/consignment-edit";
        }
        consignmentDTO.setConsignmentId(consignmentId);
        consignmentService.updateConsignment(consignmentDTO);
        return "redirect:/consignments";
    }

    @GetMapping("consignments/{consignmentId}/delete")
    public String deleteConsignment(@PathVariable("consignmentId") Long consignmentId) {
        consignmentService.deleteConsignment(consignmentId);
        return "redirect:/consignments";
    }

    @GetMapping("consignment/{consignmentId}")
    public String consignmentDetail(@PathVariable("consignmentId") Long consignmentId, Model model) {
        ConsignmentDTO consignmentDTO = consignmentService.findConsignmentById(consignmentId);
        List<ComponentOnWarehouseDTO> componentOnWarehousesByConsignmentList =
                componentOnWarehouseService.findAllComponentOnWarehousesByConsignment(consignmentId);
        model.addAttribute("consignment", consignmentDTO);
        model.addAttribute("componentOnWarehousesByConsignmentList", componentOnWarehousesByConsignmentList);
        return "consignment/consignment-detail";
    }

    @GetMapping("consignments/search")
    public String searchConsignmentsByNumber(@RequestParam(value = "query") String query, Model model) {
        List<ConsignmentDTO> consignmentsList = consignmentService.searchConsignmentsByNumber(query);
        model.addAttribute("consignmentsList", consignmentsList);
        return "consignment/consignment-list";
    }

}
