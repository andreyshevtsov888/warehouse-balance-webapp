package com.shevtsovaa.models.consignments.consignment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Repository
public interface ConsignmentRepo extends JpaRepository<Consignment, Long> {

    @Query(value = "SELECT c FROM Consignment c WHERE c.number LIKE CONCAT('%', :query, '%')")
    List<Consignment> searchConsignmentsByNumber(String query);

}
