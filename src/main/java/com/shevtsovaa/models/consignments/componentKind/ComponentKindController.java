package com.shevtsovaa.models.consignments.componentKind;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Controller
@Slf4j
public class ComponentKindController {

     @Autowired ComponentKindService componentKindService;

    @GetMapping("/componentKinds")
    public String listComponentKinds(Model model) {
        List<ComponentKindDTO> componentKindsList = componentKindService.findAllComponentKinds();
        model.addAttribute("componentKindsList", componentKindsList);
        return "componentKind/component-kind-list";
    }

    @GetMapping("componentKinds/new")
    private String createComponentKindForm(Model model) {
        ComponentKind componentKind = new ComponentKind();
        model.addAttribute("componentKind", componentKind);
        return "componentKind/component-kind-create";
    }

    @PostMapping("componentKinds/save")
    public String saveComponentKind(@Valid @ModelAttribute("componentKind") ComponentKindDTO componentKindDTO, BindingResult result, Model model) {
        model.addAttribute("componentKind", componentKindDTO);
        componentKindService.saveComponentKind(componentKindDTO);
        return "redirect:/success";
    }

    @GetMapping("componentKinds/{componentKindId}/edit")
    public String editComponentKindForm(@PathVariable("componentKindId") Long componentKindId, Model model) {
        ComponentKindDTO componentKindDTO = componentKindService.findComponentKindById(componentKindId);
        model.addAttribute("componentKind", componentKindDTO);
        return "componentKind/component-kind-edit";
    }

    @PostMapping("componentKinds/{componentKindId}/edit")
    public String updateComponentKind(@Valid @PathVariable("componentKindId") Long componentKindId,
                                      @ModelAttribute("componentKind") ComponentKindDTO componentKindDTO, BindingResult result) {
        if (result.hasErrors()) {
            return "componentKind/component-kind-edit";
        }
        componentKindDTO.setComponentKindId(componentKindId);
        componentKindService.updateComponentKind(componentKindDTO);
        return "redirect:/componentKinds";
    }

    @GetMapping("componentKinds/{componentKindId}/delete")
    public String deleteComponentKind(@PathVariable("componentKindId") Long componentKindId) {
        componentKindService.deleteComponentKind(componentKindId);
        return "redirect:/componentKinds";
    }

    @GetMapping("componentKinds/search")
    public String searchComponentKinds(@RequestParam(value = "query") String query, Model model) {
        List<ComponentKindDTO> componentKindsList = componentKindService.searchComponentKinds(query);
        model.addAttribute("componentKindsList", componentKindsList);
        return "componentKind/component-kind-list";
    }
}
