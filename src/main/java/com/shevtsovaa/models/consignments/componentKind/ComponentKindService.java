package com.shevtsovaa.models.consignments.componentKind;

import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Service
public interface ComponentKindService {

    ComponentKindDTO findComponentKindById(Long componentKindId);
    List<ComponentKindDTO> findAllComponentKinds();
    ComponentKindDTO createComponentKind(ComponentKindDTO componentKindDTO);
    void saveComponentKind(ComponentKindDTO componentKindDTO);
    ComponentKindDTO updateComponentKind(ComponentKindDTO componentKindDTO);
    void deleteComponentKind(Long componentKindId);
    List<ComponentKindDTO> searchComponentKinds(String query);
    boolean isComponentKindExistsInList(String name);
}
