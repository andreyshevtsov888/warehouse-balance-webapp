package com.shevtsovaa.models.consignments.componentKind;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Repository
public interface ComponentKindRepo extends JpaRepository<ComponentKind, Long> {



    @Query("select c from ComponentKind c where c.name = ?1")
    ComponentKind findComponentKindByName(String name);

    @Query(value = "SELECT c FROM ComponentKind c WHERE c.name LIKE CONCAT('%', :query, '%')")
    List<ComponentKind> searchComponentKind(String query);



}
