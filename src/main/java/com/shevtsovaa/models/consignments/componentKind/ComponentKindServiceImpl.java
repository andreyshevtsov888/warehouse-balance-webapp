package com.shevtsovaa.models.consignments.componentKind;

import com.shevtsovaa.exceptions.ApiRequestException;
import com.shevtsovaa.exceptions.ItemNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static com.shevtsovaa.models.consignments.componentKind.ComponentKindMapper.mapToComponentKind;
import static com.shevtsovaa.models.consignments.componentKind.ComponentKindMapper.mapToComponentKindDTO;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Service
@Slf4j
public class ComponentKindServiceImpl implements ComponentKindService {

    @Autowired
    ComponentKindRepo componentKindRepo;

    @Override
    public ComponentKindDTO findComponentKindById(Long componentKindId) {
        ComponentKind componentKind = componentKindRepo.findById(componentKindId).orElseThrow(
                () -> new ApiRequestException("The component kind could not be found."));
        log.info("Component's kind was find by id successfully.");
        return mapToComponentKindDTO(componentKind);
    }

    @Override
    public List<ComponentKindDTO> findAllComponentKinds() {
        List<ComponentKind> componentKinds = componentKindRepo.findAll();
        if(componentKinds.isEmpty()) throw new ItemNotFoundException("Non of the component's kind was fond.");
        log.info("Component kind's list was gotten from base");
        return componentKinds.stream().map(ComponentKindMapper::mapToComponentKindDTO).collect(Collectors.toList());
    }

    @Override
    public ComponentKindDTO createComponentKind(ComponentKindDTO componentKindDTO) {
        ComponentKind componentKind = new ComponentKind();
        componentKind.setName(componentKind.getName());
        ComponentKind newComponentKind = componentKindRepo.save(componentKind);

        ComponentKindDTO componentKindResponse = new ComponentKindDTO();

        componentKindResponse.setComponentKindId(newComponentKind.getComponentKindId());
        componentKindResponse.setName(newComponentKind.getName());
        return componentKindResponse;
    }

    @Override
    public void saveComponentKind(ComponentKindDTO componentKindDTO) {
        if (isComponentKindExistsInList(componentKindDTO.getName())){
            log.info("Component's kind wasn't saved.");
            throw new ApiRequestException("Component kind with that name already exists.");}
        else {
            ComponentKind componentKind = mapToComponentKind(componentKindDTO);
            componentKindRepo.save(componentKind);
            log.info("Component's kind was saved successfully.");
        }
    }

    @Override
    public ComponentKindDTO updateComponentKind(ComponentKindDTO componentKindDTO) {
        ComponentKind componentKind = mapToComponentKind(componentKindDTO);
        log.info("Component's kind was updated successfully.");
        componentKindRepo.save(componentKind);
        return componentKindDTO;
    }

    @Override
    public void deleteComponentKind(Long componentKindId) {
        componentKindRepo.findById(componentKindId).orElseThrow(() -> new ItemNotFoundException("The component kind could not be deleted."));
        try {
            componentKindRepo.deleteById(componentKindId);
        } catch (RuntimeException e) {
            throw new ApiRequestException("You can't delete this item!");
        }
        log.info("Component's kind was deleted successfully.");

    }

    @Override
    public List<ComponentKindDTO> searchComponentKinds(String query) {
        List<ComponentKind> componentKinds = componentKindRepo.searchComponentKind(query);
        return componentKinds.stream().map(ComponentKindMapper::mapToComponentKindDTO).toList();
    }

    @Override
    public boolean isComponentKindExistsInList(String name) {
        return !searchComponentKinds(name).isEmpty();
    }
}
