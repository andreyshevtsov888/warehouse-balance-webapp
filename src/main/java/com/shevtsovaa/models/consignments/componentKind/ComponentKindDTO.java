package com.shevtsovaa.models.consignments.componentKind;

import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ComponentKindDTO {

    private Long componentKindId;
    @NotEmpty(message = "Component kind should not be empty.")
    private String name;
}

