package com.shevtsovaa.models.consignments.componentKind;

import jakarta.persistence.*;
import lombok.*;

/**
 * @author Andrey Shevtsov on 22.05.2024.
 * @project warehouse-balance-webapp
 */

@Getter
@Setter
@RequiredArgsConstructor
@ToString
@Entity
@Builder
@Table(name = "component_kinds")
@AllArgsConstructor
public class ComponentKind {

    public ComponentKind(Long componentKindId) {
        this.componentKindId = componentKindId;
    }
    public ComponentKind(String name) {
        this.name = name;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Long componentKindId;
    @Column(nullable = false, length = 50, unique = true)
    private String name;

}
