package com.shevtsovaa.models.consignments.componentKind;

/**
 * @author Andrey Shevtsov on 07.06.2024.
 * @project warehouse-balance-webapp
 */

public class ComponentKindMapper {

    public static ComponentKind mapToComponentKind(ComponentKindDTO componentKindDTO) {
        return ComponentKind.builder()
                .componentKindId(componentKindDTO.getComponentKindId())
                .name(componentKindDTO.getName())
                .build();
    }

    public static ComponentKindDTO mapToComponentKindDTO(ComponentKind componentKind) {
        return ComponentKindDTO.builder()
                .componentKindId(componentKind.getComponentKindId())
                .name(componentKind.getName())
                .build();
    }
}
