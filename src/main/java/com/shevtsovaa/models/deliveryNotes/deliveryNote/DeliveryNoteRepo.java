package com.shevtsovaa.models.deliveryNotes.deliveryNote;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Repository
public interface DeliveryNoteRepo extends JpaRepository<DeliveryNote, Long> {

    @Query(value = "SELECT c FROM DeliveryNote c WHERE c.number LIKE CONCAT('%', :query, '%')")
    List<DeliveryNote> searchDeliveryNotes(String query);

}
