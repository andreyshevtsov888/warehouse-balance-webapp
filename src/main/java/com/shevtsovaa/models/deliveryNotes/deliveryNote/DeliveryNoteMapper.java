package com.shevtsovaa.models.deliveryNotes.deliveryNote;

/**
 * @author Andrey Shevtsov on 12.06.2024.
 * @project warehouse-balance-webapp
 */

public class DeliveryNoteMapper {

    public static DeliveryNote mapToDeliveryNote(DeliveryNoteDTO deliveryNoteDTO) {
        return DeliveryNote.builder()
                .deliveryNoteId(deliveryNoteDTO.getDeliveryNoteId())
                .number(deliveryNoteDTO.getNumber())
                .date(deliveryNoteDTO.getDate())
                .vatLevel(deliveryNoteDTO.getVatLevel())
                .totalSum(deliveryNoteDTO.getTotalSum())
                .contractor(deliveryNoteDTO.getContractor())
                .build();
    }


    public static DeliveryNoteDTO mapToDeliveryNoteDTO(DeliveryNote deliveryNote) {
        return DeliveryNoteDTO.builder()
                .deliveryNoteId(deliveryNote.getDeliveryNoteId())
                .number(deliveryNote.getNumber())
                .date(deliveryNote.getDate())
                .vatLevel(deliveryNote.getVatLevel())
                .totalSum(deliveryNote.getTotalSum())
                .contractor(deliveryNote.getContractor())
                .build();
    }

}
