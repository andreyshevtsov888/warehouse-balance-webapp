package com.shevtsovaa.models.deliveryNotes.deliveryNote;

import com.shevtsovaa.exceptions.ApiRequestException;
import com.shevtsovaa.exceptions.ItemNotFoundException;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import static com.shevtsovaa.models.deliveryNotes.deliveryNote.DeliveryNoteMapper.mapToDeliveryNote;
import static com.shevtsovaa.models.deliveryNotes.deliveryNote.DeliveryNoteMapper.mapToDeliveryNoteDTO;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Service
@Slf4j
public class DeliveryNoteServiceImpl implements DeliveryNoteService {

    @Autowired
    DeliveryNoteRepo deliveryNoteRepo;

    @Override
    public DeliveryNoteDTO findDeliveryNoteById(Long deliveryNoteId) {
        DeliveryNote deliveryNote = deliveryNoteRepo.findById(deliveryNoteId).orElseThrow(
                () -> new ApiRequestException("The delivery note could not be found by ID."));
        log.info("Delivery note was find by id successfully.");
        return mapToDeliveryNoteDTO(deliveryNote);
    }

    @Override
    public List<DeliveryNoteDTO> findAllDeliveryNotes() {
        List<DeliveryNote> deliveryNotes = deliveryNoteRepo.findAll();
        log.info("Delivery note's list was gotten from base");
        return deliveryNotes.stream().map(DeliveryNoteMapper::mapToDeliveryNoteDTO).collect(Collectors.toList());
    }

    @Override
    public void saveDeliveryNote(DeliveryNoteDTO deliveryNoteDTO) {
        DeliveryNote deliveryNote = mapToDeliveryNote(deliveryNoteDTO);
        deliveryNote.setTotalSum(BigDecimal.valueOf(0));
        deliveryNoteRepo.save(deliveryNote);
        log.info("Delivery note was saved successfully.");
    }

    @Override
    public void updateDeliveryNote(DeliveryNoteDTO deliveryNoteDTO) {
        DeliveryNote deliveryNote = mapToDeliveryNote(deliveryNoteDTO);
        log.info("Delivery note was updated successfully.");
        deliveryNoteRepo.save(deliveryNote);
    }

    @Override
    public void deleteDeliveryNote(Long deliveryNoteId) {
        deliveryNoteRepo.findById(deliveryNoteId).orElseThrow(() -> new ItemNotFoundException("The delivery note could not be deleted."));
        try {
            deliveryNoteRepo.deleteById(deliveryNoteId);
        } catch (RuntimeException e) {
            throw new ApiRequestException("You can't delete this item!");
        }
        log.info("Delivery note was deleted successfully.");
    }

    @Override
    public List<DeliveryNoteDTO> searchDeliveryNotes(String query) {
        List<DeliveryNote> deliveryNotes = deliveryNoteRepo.searchDeliveryNotes(query);
        return deliveryNotes.stream().map(DeliveryNoteMapper::mapToDeliveryNoteDTO).toList();
    }

    @Override
    public boolean isDeliveryNoteExistsInList(String name) {
        return !searchDeliveryNotes(name).isEmpty();
    }


}
