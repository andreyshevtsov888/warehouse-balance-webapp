package com.shevtsovaa.models.deliveryNotes.deliveryNote;

import com.shevtsovaa.models.contractors.contractor.ContractorDTO;
import com.shevtsovaa.models.contractors.contractor.ContractorService;

import com.shevtsovaa.models.deliveryNotes.ProductOnSelling.ProductOnSellingDTO;
import com.shevtsovaa.models.deliveryNotes.ProductOnSelling.ProductOnSellingService;

import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Controller
public class DeliveryNoteController {

    private final DeliveryNoteService deliveryNoteService;
    private final ContractorService contractorService;
    private final ProductOnSellingService productOnSellingService;

    @Autowired
    public DeliveryNoteController(DeliveryNoteService deliveryNoteService, ContractorService contractorService, ProductOnSellingService productOnSellingService) {
        this.deliveryNoteService = deliveryNoteService;
        this.contractorService = contractorService;
        this.productOnSellingService = productOnSellingService;
    }

    @GetMapping("/deliveryNotes")
    public String listDeliveryNotes(Model model) {
        List<DeliveryNoteDTO> deliveryNotesList = deliveryNoteService.findAllDeliveryNotes();
       model.addAttribute("deliveryNotesList", deliveryNotesList);
        return "deliveryNote/delivery-note-list";
    }

    @GetMapping("deliveryNotes/new")
    public String createDeliveryNoteForm(Model model) {
        DeliveryNote deliveryNote = new DeliveryNote();
        model.addAttribute("deliveryNote", deliveryNote);
        List<ProductOnSellingDTO> productsOnSellingList = productOnSellingService.findAllProductsOnSelling();
        model.addAttribute("productsOnSellingList", productsOnSellingList);
        List<ContractorDTO> contractorsList = contractorService.findAllContractors();
        model.addAttribute("contractorsList", contractorsList);
        return "deliveryNote/delivery-note-create";
    }

    @PostMapping("deliveryNotes/save")
    public String saveDeliveryNote(@Valid @ModelAttribute("deliveryNote") DeliveryNoteDTO deliveryNoteDTO, Model model) {
        model.addAttribute("deliveryNote", deliveryNoteDTO);
        deliveryNoteDTO.setTotalSum(BigDecimal.valueOf(0));
        deliveryNoteService.saveDeliveryNote(deliveryNoteDTO);
        return "redirect:/success";
    }

    @GetMapping("deliveryNotes/{deliveryNoteId}/edit")
    public String editDeliveryNoteForm(@PathVariable("deliveryNoteId") Long deliveryNoteId, Model model) {
        DeliveryNoteDTO deliveryNoteDTO = deliveryNoteService.findDeliveryNoteById(deliveryNoteId);
        model.addAttribute("deliveryNote", deliveryNoteDTO);
        List<ProductOnSellingDTO> productsOnSellingList = productOnSellingService.findAllProductsOnSelling();
        model.addAttribute("productsOnSellingList", productsOnSellingList);
        List<ContractorDTO> contractorsList = contractorService.findAllContractors();
        model.addAttribute("contractorsList", contractorsList);
        return "deliveryNote/delivery-note-edit";
    }

    @PostMapping("deliveryNotes/{deliveryNoteId}/edit")
    public String updateDeliveryNote(@Valid @PathVariable("deliveryNoteId") Long deliveryNoteId,
                                  @ModelAttribute("deliveryNote") DeliveryNoteDTO deliveryNoteDTO,
                                  BindingResult result) {
        if (result.hasErrors()) {
            return "deliveryNote/delivery-note-edit";
        }
        deliveryNoteDTO.setDeliveryNoteId(deliveryNoteId);
        deliveryNoteService.updateDeliveryNote(deliveryNoteDTO);
        return "redirect:/deliveryNotes";
    }

    @GetMapping("deliveryNotes/{deliveryNoteId}/delete")
    public String deleteDeliveryNote(@PathVariable("deliveryNoteId") Long deliveryNoteId) {
        deliveryNoteService.deleteDeliveryNote(deliveryNoteId);
        return "redirect:/deliveryNotes";
    }

    @GetMapping("deliveryNote/{deliveryNoteId}")
    public String deliveryNoteDetail(@PathVariable("deliveryNoteId") Long deliveryNoteId, Model model) {
        DeliveryNoteDTO deliveryNoteDTO = deliveryNoteService.findDeliveryNoteById(deliveryNoteId);
        List<ProductOnSellingDTO> productsOnSellingByDeliveryNoteList =
                productOnSellingService.findAllProductsOnSellingByDeliveryNote(deliveryNoteId);
        model.addAttribute("deliveryNote", deliveryNoteDTO);
        model.addAttribute("productsOnSellingByDeliveryNoteList", productsOnSellingByDeliveryNoteList);
        return "deliveryNote/delivery-note-detail";
    }

    @GetMapping("deliveryNotes/search")
    public String searchDeliveryNotes(@RequestParam(value = "query") String query, Model model) {
        List<DeliveryNoteDTO> deliveryNotesList = deliveryNoteService.searchDeliveryNotes(query);
        model.addAttribute("deliveryNotesList", deliveryNotesList);
        return "deliveryNote/delivery-note-list";
    }

}
