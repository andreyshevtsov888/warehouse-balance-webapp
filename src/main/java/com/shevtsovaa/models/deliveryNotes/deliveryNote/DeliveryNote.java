package com.shevtsovaa.models.deliveryNotes.deliveryNote;

import com.shevtsovaa.models.contractors.contractor.Contractor;

import jakarta.persistence.*;
import lombok.*;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Andrey Shevtsov on 22.05.2024.
 * @project warehouse-balance-webapp
 */

@Getter
@Setter
@ToString
@Builder
@RequiredArgsConstructor
@Entity
@Table(name = "delivery_notes")
@AllArgsConstructor
public class DeliveryNote {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Long deliveryNoteId;
    @Column(nullable = false)
    private String number;
    @Column(nullable = false)
    @Temporal(TemporalType.DATE)
    private Date date;
    @Column(nullable = false, columnDefinition = "integer default 20")
    private int vatLevel;
    @Column
    private BigDecimal totalSum;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "contractor_id")
    @ToString.Exclude
    private Contractor contractor;

}


