package com.shevtsovaa.models.deliveryNotes.deliveryNote;

import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Service
public interface DeliveryNoteService {

    DeliveryNoteDTO findDeliveryNoteById(Long deliveryNoteId);
    List<DeliveryNoteDTO> findAllDeliveryNotes();
    void saveDeliveryNote(DeliveryNoteDTO deliveryNoteDTO);
    void updateDeliveryNote(DeliveryNoteDTO deliveryNoteDTO);
    void deleteDeliveryNote(Long deliveryNoteId);
    List<DeliveryNoteDTO> searchDeliveryNotes(String query);
    boolean isDeliveryNoteExistsInList(String name);

}
