package com.shevtsovaa.models.deliveryNotes.product;

import com.shevtsovaa.models.consignments.componentOnWarehouse.ComponentOnWarehouse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Repository
public interface ProductRepo extends JpaRepository<Product, Long> {

    @Query(value = "SELECT c FROM Product c WHERE c.name LIKE CONCAT('%', :query, '%')")
    List<Product> searchProducts(String query);

}
