package com.shevtsovaa.models.deliveryNotes.product;
;
import com.shevtsovaa.models.warehouse.componentsInProducts.ComponentInProduct;
import com.shevtsovaa.models.warehouse.maesureUnit.MeasureUnit;
import jakarta.validation.constraints.NotEmpty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Data
@Builder
public class ProductDTO {

    private Long productId;
    @NotEmpty(message = "Product name should not be empty.")
    private String name;
    private String description;
    private MeasureUnit measureUnit;
    private List<ComponentInProduct> componentsInProductList;

}
