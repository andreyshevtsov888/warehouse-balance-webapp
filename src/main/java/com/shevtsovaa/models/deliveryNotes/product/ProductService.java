package com.shevtsovaa.models.deliveryNotes.product;

import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Service
public interface ProductService {

    ProductDTO findProductById(Long productId);
    List<ProductDTO> findAllProducts();
    void saveProduct(ProductDTO productDTO);
    void updateProduct(ProductDTO productDTO);
    void deleteProduct(Long productId);
    List<ProductDTO> searchProducts(String query);
    boolean isProductExistsInList(String name);
//    Product assignComponentToProduct(Long productId, Long componentId);

}
