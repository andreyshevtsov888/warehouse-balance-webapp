package com.shevtsovaa.models.deliveryNotes.product;

import com.shevtsovaa.exceptions.ApiRequestException;
import com.shevtsovaa.exceptions.ItemNotFoundException;
import com.shevtsovaa.models.consignments.component.ComponentRepo;
import com.shevtsovaa.models.consignments.componentOnWarehouse.ComponentOnWarehouseRepo;
import com.shevtsovaa.models.consignments.componentOnWarehouse.ComponentOnWarehouseService;
import com.shevtsovaa.models.deliveryNotes.deliveryNote.DeliveryNoteRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static com.shevtsovaa.models.deliveryNotes.product.ProductMapper.mapToProduct;
import static com.shevtsovaa.models.deliveryNotes.product.ProductMapper.mapToProductDTO;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Service
@Slf4j
public class ProductServiceImpl implements ProductService {

    private final ComponentOnWarehouseRepo componentOnWarehouseRepo;
    private final ComponentRepo componentRepo;
    private final ProductRepo productRepo;
    private final DeliveryNoteRepo deliveryNoteRepo;
    private final ComponentOnWarehouseService componentOnWarehouseService;

    @Autowired
    public ProductServiceImpl(ComponentOnWarehouseRepo componentOnWarehouseRepo, ComponentRepo componentRepo,
                              ProductRepo productRepo, DeliveryNoteRepo deliveryNoteRepo,
                              ComponentOnWarehouseService componentOnWarehouseService) {
        this.componentOnWarehouseRepo = componentOnWarehouseRepo;
        this.componentRepo = componentRepo;
        this.productRepo = productRepo;
        this.deliveryNoteRepo = deliveryNoteRepo;
        this.componentOnWarehouseService = componentOnWarehouseService;
    }



    @Override
    public ProductDTO findProductById(Long productId) {
        Product product = productRepo.findById(productId).orElseThrow(
                () -> new ApiRequestException("The product could not be found by ID."));
        log.info("Product was find by id successfully.");
        return mapToProductDTO(product);
    }

    @Override
    public List<ProductDTO> findAllProducts() {
        List<Product> products = productRepo.findAll();
        log.info("Product's list was got from base");
        return products.stream().map(ProductMapper::mapToProductDTO).collect(Collectors.toList());
    }


    @Override
    public void saveProduct(ProductDTO productDTO) {
        Product product = mapToProduct(productDTO);

 //       writeOffComponents(productDTO.getQuantity());

        productRepo.save(product);
        log.info("Product was saved successfully.");

    }

    @Override
    public void updateProduct(ProductDTO productDTO) {
        Product product = mapToProduct(productDTO);
        log.info("Product was updated successfully.");
        productRepo.save(product);
    }

    @Override
    public void deleteProduct(Long productId) {
        productRepo.findById(productId).orElseThrow(() -> new ItemNotFoundException("The product could not be deleted."));
        try {
            productRepo.deleteById(productId);
        } catch (RuntimeException e) {
            throw new ApiRequestException("You can't delete this item!");
        }
        log.info("Product was deleted successfully.");
    }

    @Override
    public List<ProductDTO> searchProducts(String query) {
        List<Product> products = productRepo.searchProducts(query);
        return products.stream().map(ProductMapper::mapToProductDTO).toList();
    }

    @Override
    public boolean isProductExistsInList(String name) {
        return !searchProducts(name).isEmpty();
    }

//    @Override
//    public Product assignComponentToProduct(Long productId, Long componentId) {
//        List<Component> componentsList = null;
//        Product product = productRepo.findById(productId).get();
//        Component component = componentRepo.findById(componentId).get();
//        product.getComponentsList();
//        componentsList.add(component);
//        product.setComponentsList(componentsList);
//        return productRepo.save(product);
//
//    }

//    public void writeOffComponents(Integer productsQuantity) {
//        List<ComponentOnWarehouse> componentOnWarehousesList = componentOnWarehouseRepo.findAll();
//        for(ComponentOnWarehouse comp: componentOnWarehousesList) {
//            Float quantityOnProduct = comp.getComponent().getComponentsInProduct();
//            float quantityToWriteOff = quantityOnProduct*productsQuantity;
//            List<ComponentOnWarehouse> componentOnWarehouseByComponentList =
//                    componentOnWarehouseRepo.getComponentOnWarehouseByComponent(comp.getComponent().getComponentId());
//            while (!componentOnWarehouseByComponentList.isEmpty()) {
//                for(ComponentOnWarehouse comp2 : componentOnWarehouseByComponentList) {
//                    if((comp2.getQuantity()).floatValue() < quantityToWriteOff) {
//                        componentOnWarehouseService.deleteComponentOnWarehouse(comp2.getComponentOnWarehouseId());
//                    }
//                    else { comp2.setQuantity(BigDecimal.valueOf((comp.getQuantity()).floatValue() - quantityToWriteOff));
//                        componentOnWarehouseRepo.save(comp2);
//                    }
//                }
//            }
//
//        }
//    }
}
