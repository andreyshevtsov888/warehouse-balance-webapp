package com.shevtsovaa.models.deliveryNotes.product;

/**
 * @author Andrey Shevtsov on 12.06.2024.
 * @project warehouse-balance-webapp
 */

public class ProductMapper {

    public static Product mapToProduct(ProductDTO productDTO) {
        return Product.builder()
                .productId(productDTO.getProductId())
                .name(productDTO.getName())
                .description(productDTO.getDescription())
                .measureUnit(productDTO.getMeasureUnit())
                .build();
    }


    public static ProductDTO mapToProductDTO(Product product) {
        return ProductDTO.builder()
                .productId(product.getProductId())
                .name(product.getName())
                .description(product.getDescription())
                .measureUnit(product.getMeasureUnit())
                .build();
    }
}
