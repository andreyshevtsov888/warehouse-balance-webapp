package com.shevtsovaa.models.deliveryNotes.product;

import com.shevtsovaa.models.warehouse.maesureUnit.MeasureUnit;
import jakarta.persistence.*;
import lombok.*;

/**
 * @author Andrey Shevtsov on 22.05.2024.
 * @project warehouse-balance-webapp
 */


@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Builder
@Entity
@Table(name = "products")
@AllArgsConstructor
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Long productId;
    @Column(nullable = false, length = 50)
    private String name;
    @Column(length = 150)
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "measure_unit_id")
    @ToString.Exclude
    private MeasureUnit measureUnit;

}
