package com.shevtsovaa.models.deliveryNotes.product;

import com.shevtsovaa.models.warehouse.componentsInProducts.ComponentInProductDTO;
import com.shevtsovaa.models.warehouse.componentsInProducts.ComponentInProductService;
import com.shevtsovaa.models.warehouse.maesureUnit.MeasureUnitDTO;
import com.shevtsovaa.models.warehouse.maesureUnit.MeasureUnitService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Controller
public class ProductController {

    private final ProductService productService;
    private final MeasureUnitService measureUnitService;
    private final ComponentInProductService componentInProductService;

    @Autowired
    public ProductController(ProductService productService, MeasureUnitService measureUnitService,
                             ComponentInProductService componentInProductService) {
        this.productService = productService;
        this.measureUnitService = measureUnitService;
        this.componentInProductService = componentInProductService;
    }

    @GetMapping("/products")
    public String listProducts(Model model) {
        List<ProductDTO> productsList = productService.findAllProducts();
        model.addAttribute("productsList", productsList);
        return "product/product-list";
    }

    @GetMapping("products/new")
    public String createProductForm(Model model) {
        Product product = new Product();
        model.addAttribute("product", product);
        List<MeasureUnitDTO> measureUnitsList = measureUnitService.findAllMeasureUnits();
        model.addAttribute("measureUnitsList", measureUnitsList);
        return "product/product-create";
    }

    @PostMapping("products/save")
    public String saveProduct(@Valid @ModelAttribute("product") ProductDTO productDTO, BindingResult result, Model model) {
        model.addAttribute("product", productDTO);
        productService.saveProduct(productDTO);
        return "redirect:/products";
    }

    @GetMapping("products/{productId}/edit")
    public String editProductForm(@PathVariable("productId") Long productId, Model model) {
        ProductDTO productDTO = productService.findProductById(productId);
        model.addAttribute("product", productDTO);
        List<MeasureUnitDTO> measureUnitsList = measureUnitService.findAllMeasureUnits();
        model.addAttribute("measureUnitsList", measureUnitsList);
        return "product/product-edit";
    }

    @PostMapping("products/{productId}/update")
    public String updateProduct(@Valid @PathVariable("productId") Long productId,
                                  @ModelAttribute("product") ProductDTO productDTO,
                                  BindingResult result) {
        if (result.hasErrors()) {
            return "product/product-edit";
        }
        productDTO.setProductId(productId);
        productService.updateProduct(productDTO);
        return "redirect:/products";
    }

    @GetMapping("products/{productId}")
    public String productDetail(@PathVariable("productId") Long productId, Model model) {
        ProductDTO productDTO = productService.findProductById(productId);
        List<ComponentInProductDTO> componentsInProductByProductList =
                componentInProductService.findAllComponentsInProductByProduct(productId);
        model.addAttribute("product", productDTO);
        model.addAttribute("componentsInProductByProductList", componentsInProductByProductList);
        return "product/product-detail";
    }

    @GetMapping("products/{productId}/delete")
    public String deleteProduct(@PathVariable("productId") Long productId) {
        productService.deleteProduct(productId);
        return "redirect:/products";
    }

    @GetMapping("products/search")
    public String searchProducts(@RequestParam(value = "query") String query, Model model) {
        List<ProductDTO> productsList = productService.searchProducts(query);
        model.addAttribute("productsList", productsList);
        return "product/product-list";
    }


}
