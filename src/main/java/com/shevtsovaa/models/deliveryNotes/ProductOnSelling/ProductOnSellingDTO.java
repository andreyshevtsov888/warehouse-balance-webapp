package com.shevtsovaa.models.deliveryNotes.ProductOnSelling;

import com.shevtsovaa.models.deliveryNotes.deliveryNote.DeliveryNote;
import com.shevtsovaa.models.deliveryNotes.product.Product;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Data
@Builder
public class ProductOnSellingDTO {

    private Long productOnSellingId;
    private BigDecimal quantity;
    private BigDecimal price;
    private Boolean isSendToSelling;
    private Product product;
    private DeliveryNote deliveryNote;

}
