package com.shevtsovaa.models.deliveryNotes.ProductOnSelling;

import com.shevtsovaa.exceptions.ApiRequestException;
import com.shevtsovaa.exceptions.ItemNotFoundException;
import com.shevtsovaa.models.deliveryNotes.deliveryNote.DeliveryNote;
import com.shevtsovaa.models.deliveryNotes.deliveryNote.DeliveryNoteRepo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import static com.shevtsovaa.models.deliveryNotes.ProductOnSelling.ProductOnSellingMapper.mapToProductOnSelling;
import static com.shevtsovaa.models.deliveryNotes.ProductOnSelling.ProductOnSellingMapper.mapToProductOnSellingDTO;


/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Service
@Slf4j
public class ProductOnSellingServiceImpl implements ProductOnSellingService {

    private final ProductOnSellingRepo productOnSellingRepo;
    private final DeliveryNoteRepo deliveryNoteRepo;

    @Autowired
    public ProductOnSellingServiceImpl(ProductOnSellingRepo productOnSellingRepo, DeliveryNoteRepo deliveryNoteRepo) {
        this.productOnSellingRepo = productOnSellingRepo;
        this.deliveryNoteRepo = deliveryNoteRepo;
    }


    @Override
    public ProductOnSellingDTO findProductOnSellingById(Long productOnSellingId) {
        ProductOnSelling productOnSelling = productOnSellingRepo.findById(productOnSellingId).orElseThrow(
                () -> new ApiRequestException("The product on selling could not be found by ID."));
        log.info("Product on selling was find by id successfully.");
        return mapToProductOnSellingDTO(productOnSelling);
    }

    @Override
    public List<ProductOnSellingDTO> findAllProductsOnSelling() {
        List<ProductOnSelling> productsOnSellingList = productOnSellingRepo.findAll();
        log.info("Product's on selling list was got from base");
        return productsOnSellingList.stream().map(ProductOnSellingMapper::mapToProductOnSellingDTO).collect(Collectors.toList());
    }

    @Override
    public List<ProductOnSellingDTO> findAllProductsOnSellingByDeliveryNote(Long deliveryNoteId) {
        List<ProductOnSelling> productsOnSellingByDeliveryNoteList = productOnSellingRepo.searchProductsOnSellingByDeliveryNote(deliveryNoteId);
        log.info("Product on selling by delivery note list was gotten from base");
        return productsOnSellingByDeliveryNoteList.stream().map(ProductOnSellingMapper::mapToProductOnSellingDTO).collect(Collectors.toList());
    }

    @Override
    public void createProductOnSelling(Long deliveryNoteId, ProductOnSellingDTO productOnSellingDTO) {
        DeliveryNote deliveryNote = deliveryNoteRepo.findById(deliveryNoteId).orElseThrow(
                () -> new ApiRequestException("The delivery note could not be found by ID."));
        ProductOnSelling productOnSelling = mapToProductOnSelling(productOnSellingDTO);
        addTotalSumToDeliveryNote(deliveryNoteId, productOnSelling);
        productOnSelling.setDeliveryNote(deliveryNote);
        productOnSellingRepo.save(productOnSelling);
        log.info("Product on selling was saved successfully.");
    }


    @Override
    public void updateProductOnSelling(ProductOnSellingDTO productOnSellingDTO) {
        ProductOnSelling productOnSelling = mapToProductOnSelling(productOnSellingDTO);
        if (productOnSelling.getIsSendToSelling())
            throw new ApiRequestException("You can't edit this item already");
        ProductOnSelling posBefore = productOnSellingRepo.findById(productOnSelling.getProductOnSellingId()).orElseThrow(
                () -> new ApiRequestException("The product on selling could not be found by ID."));
        BigDecimal sumBefore = posBefore.getPrice().multiply(posBefore.getQuantity());
        BigDecimal sumAfter = productOnSelling.getPrice().multiply(productOnSelling.getQuantity());
        Long deliveryNoteId = productOnSelling.getDeliveryNote().getDeliveryNoteId();
        updateTotalSumInDeliveryNote(sumBefore, sumAfter, deliveryNoteId);
        productOnSellingRepo.save(productOnSelling);
        log.info("Product on selling was updated successfully.");
    }

    @Override
    public void deleteProductOnSelling(Long productOnSellingId) {
        if (productOnSellingRepo.findById(productOnSellingId).orElseThrow(
                () -> new ApiRequestException("The product on selling could not be found by ID.")).getIsSendToSelling())
            throw new ApiRequestException("You can't delete this item already");
        productOnSellingRepo.findById(productOnSellingId)
                .orElseThrow(() -> new ItemNotFoundException("The product on selling could not be deleted."));
        subtractTotalSumFromDeliveryNote(productOnSellingId);
        try {
            productOnSellingRepo.deleteById(productOnSellingId);
        } catch (RuntimeException e) {
            throw new ApiRequestException("You can't delete this item!");
        }
        log.info("Product on selling was deleted successfully.");
    }

    @Override
    public void addTotalSumToDeliveryNote(Long deliveryNoteId, ProductOnSelling productOnSelling) {
        DeliveryNote deliveryNote = deliveryNoteRepo.findById(deliveryNoteId)
                .orElseThrow(() -> new ItemNotFoundException("Could not find this delivery note."));
        deliveryNote.setTotalSum(deliveryNote.getTotalSum().add(productOnSelling.getQuantity().multiply(productOnSelling.getPrice())));

        log.info("New total amount was set on :  {}", deliveryNote.getTotalSum());
    }

    @Override
    public void subtractTotalSumFromDeliveryNote(Long productOnSellingId) {
        ProductOnSelling productOnSelling = productOnSellingRepo.findById(productOnSellingId)
                .orElseThrow(() -> new ItemNotFoundException("Could not find this product on selling."));
        DeliveryNote deliveryNote = deliveryNoteRepo.findById(productOnSelling.getDeliveryNote().getDeliveryNoteId())
                .orElseThrow(() -> new ItemNotFoundException("Could not find this delivery note."));
        deliveryNote.setTotalSum(deliveryNote.getTotalSum().subtract(productOnSelling.getQuantity().multiply(productOnSelling.getPrice())));
    }

    @Override
    public void updateTotalSumInDeliveryNote(BigDecimal before, BigDecimal after, Long deliveryNoteId) {
        DeliveryNote deliveryNote = deliveryNoteRepo.findById(deliveryNoteId)
                .orElseThrow(() -> new ItemNotFoundException("Could not find this delivery note."));
        BigDecimal gap = after.subtract(before);
        deliveryNote.setTotalSum(deliveryNote.getTotalSum().add(gap));
    }


}
