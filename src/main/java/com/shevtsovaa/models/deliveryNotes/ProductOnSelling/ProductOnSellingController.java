package com.shevtsovaa.models.deliveryNotes.ProductOnSelling;

import com.shevtsovaa.models.deliveryNotes.deliveryNote.DeliveryNoteDTO;
import com.shevtsovaa.models.deliveryNotes.deliveryNote.DeliveryNoteService;
import com.shevtsovaa.models.deliveryNotes.product.ProductDTO;
import com.shevtsovaa.models.deliveryNotes.product.ProductService;

import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Controller
public class ProductOnSellingController {

    private final ProductOnSellingService productOnSellingService;
    private final ProductService productService;
    private final DeliveryNoteService deliveryNoteService;

    @Autowired
    public ProductOnSellingController(ProductOnSellingService productOnSellingService, ProductService productService, DeliveryNoteService deliveryNoteService) {
        this.productOnSellingService = productOnSellingService;
        this.productService = productService;
        this.deliveryNoteService = deliveryNoteService;
    }


    @GetMapping("/productsOnSelling")
    public String listProductsOnSelling(Model model) {
        List<ProductOnSellingDTO> productsOnSellingList = productOnSellingService.findAllProductsOnSelling();
        model.addAttribute("productsOnSellingList", productsOnSellingList);
        return "productOnSelling/product-on-selling-list";
    }

    @GetMapping("productsOnSelling/{deliveryNoteId}/new")
    public String createProductOnSellingForm(@PathVariable("deliveryNoteId") Long deliveryNoteId, Model model) {
        ProductOnSelling productOnSelling = new ProductOnSelling();
        model.addAttribute("productOnSelling", productOnSelling);
        model.addAttribute("deliveryNoteId", deliveryNoteId);
        List<ProductDTO> productsList = productService.findAllProducts();
        model.addAttribute("productsList", productsList);
        return "productOnSelling/product-on-selling-create";
    }

    @PostMapping("productsOnSelling/{deliveryNoteId}")
    public String createProductOnSelling(@PathVariable("deliveryNoteId") Long deliveryNoteId,
                                         @ModelAttribute("deliveryNote") ProductOnSellingDTO productOnSellingDTO, Model model) {
        model.addAttribute("deliveryNoteId", deliveryNoteId);
        model.addAttribute("productOnSelling", productOnSellingDTO);
        productOnSellingDTO.setIsSendToSelling(false);
        productOnSellingService.createProductOnSelling(deliveryNoteId, productOnSellingDTO);
        return "redirect:/deliveryNote/{deliveryNoteId}";
    }

    @GetMapping("productsOnSelling/{productOnSellingId}/edit")
    public String editProductOnSellingForm(@PathVariable("productOnSellingId") Long productOnSellingId, Model model) {
        ProductOnSellingDTO productOnSellingDTO = productOnSellingService.findProductOnSellingById(productOnSellingId);
        model.addAttribute("productOnSelling", productOnSellingDTO);
        List<ProductDTO> productsList = productService.findAllProducts();
        model.addAttribute("productsList", productsList);
        List<DeliveryNoteDTO> deliveryNotesList = deliveryNoteService.findAllDeliveryNotes();
        model.addAttribute("deliveryNotesList", deliveryNotesList);
        return "productOnSelling/product-on-selling-edit";
    }

    @PostMapping("productsOnSelling/{productOnSellingId}/update")
    public String updateProductOnSelling(@Valid @PathVariable("productOnSellingId") Long productOnSellingId,
                                  @ModelAttribute("productOnSelling") ProductOnSellingDTO productOnSellingDTO,
                                  BindingResult result) {
        if (result.hasErrors()) {
            return "productOnSelling/product-on-selling-edit";
        }
        ProductOnSellingDTO productOnSellingDTO1 = productOnSellingService.findProductOnSellingById(productOnSellingId);
        productOnSellingDTO.setProductOnSellingId(productOnSellingId);
        productOnSellingDTO.setIsSendToSelling(productOnSellingDTO1.getIsSendToSelling());
        productOnSellingDTO.setDeliveryNote(productOnSellingDTO1.getDeliveryNote());
        productOnSellingService.updateProductOnSelling(productOnSellingDTO);
        return "redirect:/productsOnSelling";
    }

    @GetMapping("productsOnSelling/{productOnSellingId}/delete")
    public String deleteProductOnSelling(@PathVariable("productOnSellingId") Long productOnSellingId) {
        productOnSellingService.deleteProductOnSelling(productOnSellingId);
        return "redirect:/productsOnSelling";
    }

}
