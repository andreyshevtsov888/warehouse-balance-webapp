package com.shevtsovaa.models.deliveryNotes.ProductOnSelling;


/**
 * @author Andrey Shevtsov on 12.06.2024.
 * @project warehouse-balance-webapp
 */

public class ProductOnSellingMapper {

    public static ProductOnSelling mapToProductOnSelling(ProductOnSellingDTO productOnSellingDTO) {
        return ProductOnSelling.builder()
                .productOnSellingId(productOnSellingDTO.getProductOnSellingId())
                .quantity(productOnSellingDTO.getQuantity())
                .price(productOnSellingDTO.getPrice())
                .isSendToSelling(productOnSellingDTO.getIsSendToSelling())
                .deliveryNote(productOnSellingDTO.getDeliveryNote())
                .product(productOnSellingDTO.getProduct())
                .build();
    }

    public static ProductOnSellingDTO mapToProductOnSellingDTO(ProductOnSelling productOnSelling) {
        return ProductOnSellingDTO.builder()
                .productOnSellingId(productOnSelling.getProductOnSellingId())
                .quantity(productOnSelling.getQuantity())
                .price(productOnSelling.getPrice())
                .isSendToSelling(productOnSelling.getIsSendToSelling())
                .deliveryNote(productOnSelling.getDeliveryNote())
                .product(productOnSelling.getProduct())
                .build();
    }
}
