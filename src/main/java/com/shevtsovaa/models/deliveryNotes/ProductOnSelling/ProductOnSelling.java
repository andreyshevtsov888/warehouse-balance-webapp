package com.shevtsovaa.models.deliveryNotes.ProductOnSelling;

import com.shevtsovaa.models.deliveryNotes.deliveryNote.DeliveryNote;
import com.shevtsovaa.models.deliveryNotes.product.Product;

import jakarta.persistence.*;
import lombok.*;

import java.math.BigDecimal;

/**
 * @author Andrey Shevtsov on 22.05.2024.
 * @project warehouse-balance-webapp
 */


@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Builder
@Entity
@Table(name = "products_on_selling")
@AllArgsConstructor
public class ProductOnSelling {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Long productOnSellingId;
    @Column(nullable = false)
    private BigDecimal quantity;
    @Column(nullable = false)
    private BigDecimal price;
    @Column(columnDefinition="BOOLEAN DEFAULT false")
    private Boolean isSendToSelling;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_id")
    @ToString.Exclude
    private Product product;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "delivery_note_id")
    @ToString.Exclude
    private DeliveryNote deliveryNote;


}
