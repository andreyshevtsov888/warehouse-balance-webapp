package com.shevtsovaa.models.deliveryNotes.ProductOnSelling;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Repository
public interface ProductOnSellingRepo extends JpaRepository<ProductOnSelling, Long> {


    @Query(value = "FROM ProductOnSelling WHERE deliveryNote.deliveryNoteId = :query")
    List<ProductOnSelling> searchProductsOnSellingByDeliveryNote(Long query);

    @Query(value = "FROM ProductOnSelling WHERE deliveryNote.deliveryNoteId = :query AND isSendToSelling = false")
    List<ProductOnSelling> searchProductsOnSellingByDeliveryNoteNotWroteYet(Long query);

}
