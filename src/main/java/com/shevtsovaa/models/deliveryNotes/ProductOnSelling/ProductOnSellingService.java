package com.shevtsovaa.models.deliveryNotes.ProductOnSelling;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Service
public interface ProductOnSellingService {

    ProductOnSellingDTO findProductOnSellingById(Long ProductOnSellingId);
    List<ProductOnSellingDTO> findAllProductsOnSelling();
    List<ProductOnSellingDTO> findAllProductsOnSellingByDeliveryNote(Long deliveryNoteId);
    void updateProductOnSelling(ProductOnSellingDTO productOnSellingDTO);
    void deleteProductOnSelling(Long productOnSellingId);
    void createProductOnSelling(Long deliveryNoteId, ProductOnSellingDTO productOnSellingDTO);
    void addTotalSumToDeliveryNote(Long deliveryNoteId, ProductOnSelling productOnSelling);
    void subtractTotalSumFromDeliveryNote(Long productOnSellingId);
    void updateTotalSumInDeliveryNote(BigDecimal before, BigDecimal after, Long deliveryNoteId);
}
