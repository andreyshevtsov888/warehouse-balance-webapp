package com.shevtsovaa.models;

import lombok.Data;
import lombok.Getter;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */


@Getter
public enum ContractorType {

    SUPPLIER("Supplier"),
    CONSUMER("Customer"),
    TRANSPORT("Transport"),
    INSURANCE("Insurance");

    private final String displayText;

    ContractorType(String displayText) {
        this.displayText = displayText;
    }

}
