package com.shevtsovaa.models.warehouse.warehouseBalance;

import com.shevtsovaa.DTO.SumWarehouseBalanceGroupByDTO;
import com.shevtsovaa.exceptions.ApiRequestException;
import com.shevtsovaa.exceptions.ItemNotFoundException;
import com.shevtsovaa.models.consignments.componentOnWarehouse.ComponentOnWarehouse;
import com.shevtsovaa.models.consignments.componentOnWarehouse.ComponentOnWarehouseRepo;
import com.shevtsovaa.models.consignments.consignment.ConsignmentRepo;
import com.shevtsovaa.models.deliveryNotes.ProductOnSelling.ProductOnSelling;
import com.shevtsovaa.models.deliveryNotes.ProductOnSelling.ProductOnSellingRepo;
import com.shevtsovaa.models.warehouse.componentsInProducts.ComponentInProduct;
import com.shevtsovaa.models.warehouse.componentsInProducts.ComponentInProductRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import static com.shevtsovaa.models.warehouse.warehouseBalance.WarehouseBalanceMapper.*;

/**
 * @author Andrey Shevtsov on 05.06.2024.
 * @project warehouse-balance-webapp
 */

@Service
@Slf4j
public class WarehouseBalanceServiceImpl implements WarehouseBalanceService {

    private final WarehouseBalanceRepo warehouseBalanceRepo;
    private final ComponentOnWarehouseRepo componentOnWarehouseRepo;
    private final ConsignmentRepo consignmentRepo;
    private final ProductOnSellingRepo productOnSellingRepo;
    private final ComponentInProductRepo componentInProductRepo;

    @Autowired
    public WarehouseBalanceServiceImpl(WarehouseBalanceRepo warehouseBalanceRepo, ComponentOnWarehouseRepo componentOnWarehouseRepo,
                                       ConsignmentRepo consignmentRepo, ProductOnSellingRepo productOnSellingRepo, ComponentInProductRepo componentInProductRepo) {
        this.warehouseBalanceRepo = warehouseBalanceRepo;
        this.componentOnWarehouseRepo = componentOnWarehouseRepo;
        this.consignmentRepo = consignmentRepo;
        this.productOnSellingRepo = productOnSellingRepo;
        this.componentInProductRepo = componentInProductRepo;
    }

    @Override
    public WarehouseBalanceDTO findWarehouseBalanceById(Long warehouseBalanceId) {
        WarehouseBalance warehouseBalance = warehouseBalanceRepo.findById(warehouseBalanceId).orElseThrow(
                () -> new ApiRequestException("The warehouse balance could not be found by ID."));
        log.info("Warehouse balance was find by id successfully.");
        return mapToWarehouseBalanceDTO(warehouseBalance);
    }

    @Override
    public List<WarehouseBalanceDTO> findAllWarehouseBalances() {
        List<WarehouseBalance> warehouseBalancesList = warehouseBalanceRepo.findAll();
        log.info("Warehouse balance list was gotten from base");
        return warehouseBalancesList.stream().map(WarehouseBalanceMapper::mapToWarehouseBalanceDTO).collect(Collectors.toList());
    }

    @Override
    public void saveWarehouseBalance(WarehouseBalanceDTO warehouseBalanceDTO) {
        WarehouseBalance warehouseBalance = mapToWarehouseBalance(warehouseBalanceDTO);
        warehouseBalanceRepo.save(warehouseBalance);
        log.info("Warehouse balance was saved successfully.");
    }


    @Override
    public void updateWarehouseBalance(WarehouseBalanceDTO warehouseBalanceDTO) {
        WarehouseBalance warehouseBalance = mapToWarehouseBalance(warehouseBalanceDTO);
        log.info("Warehouse balance was updated successfully.");
        warehouseBalanceRepo.save(warehouseBalance);
    }

    @Override
    public void deleteWarehouseBalance(Long componentOnWarehouseId) {
        warehouseBalanceRepo.findById(componentOnWarehouseId).orElseThrow(() -> new ItemNotFoundException("The warehouseBalance could not be deleted."));
        try {
            warehouseBalanceRepo.deleteById(componentOnWarehouseId);
        } catch (RuntimeException e) {
            throw new ApiRequestException("You can't delete this item!");
        }
        log.info("Warehouse balance was deleted successfully.");
    }

    @Override
    public void sendAllComponentsOnWarehouseToWarehouseBalance(Long consignmentId) {
        List<ComponentOnWarehouse> componentsOnWarehousesListByConsignment
                = componentOnWarehouseRepo.findComponentOnWarehouseByConsignment(consignmentId);
        for (ComponentOnWarehouse cow : componentsOnWarehousesListByConsignment) {
            cow.setIsSendToWarehouse(true);
            WarehouseBalance warehouseBalance = mapToWarehouseBalanceFromCOW(cow);
            warehouseBalance.setWarehouseBalanceId(cow.getComponentOnWarehouseId());
            warehouseBalance.setConsignment(consignmentRepo.findById(consignmentId).orElseThrow(() -> new ApiRequestException("Can't find consignment by id")));
            warehouseBalanceRepo.save(warehouseBalance);
            log.info("Warehouse balance was saved successfully");
        }
    }

    @Override
    public List<SumWarehouseBalanceGroupByDTO> findAllWarehouseBalancesGroupByComponent() {
        return warehouseBalanceRepo.sumWarehouseBalancesGroupByComponent();
    }

    @Override
    public void writeOffWarehouseBalanceToSelling(Long deliveryNoteId) {
        List<ProductOnSelling> productOnSellingByDeliveryNoteList = productOnSellingRepo.searchProductsOnSellingByDeliveryNote(deliveryNoteId);
        for (ProductOnSelling pos : productOnSellingByDeliveryNoteList) {
            log.info("{} pos", pos.toString());
            pos.setIsSendToSelling(true);
            productOnSellingRepo.save(pos);
            log.info("{} pos after setting true", pos);
            BigDecimal productQuantityToSelling = pos.getQuantity();
            log.info("{} productQuantityToSelling", productQuantityToSelling.toString());
            Long productId = pos.getProduct().getProductId();
            log.info("{} productId", productId.toString());
            List<ComponentInProduct> componentsInProductByProductList = componentInProductRepo.findComponentInProductByProduct(productId);
            for (ComponentInProduct cip : componentsInProductByProductList) {
                log.info("{} cip", cip.toString());
                BigDecimal componentQuantityByProduct = BigDecimal.valueOf(cip.getQuantityByProduct());
                log.info("{} componentQuantityByProduct", componentQuantityByProduct);
                BigDecimal componentQuantityToWriteOff = productQuantityToSelling.multiply(componentQuantityByProduct);
                log.info("{} componentQuantityToWriteOff", componentQuantityToWriteOff);
                Long componentId = cip.getComponent().getComponentId();
                log.info("{} componentId", componentId.toString());
                List<WarehouseBalance> warehouseBalanceList = warehouseBalanceRepo.findWarehouseBalanceByComponent(componentId);
                for (WarehouseBalance wb : warehouseBalanceList) {
                    log.info("{} wb", wb.toString());
                    Long warehouseBalanceId = wb.getWarehouseBalanceId();
                    log.info("{} warehouseBalanceId", warehouseBalanceId.toString());
                    BigDecimal quantityComponentsInWarehouseBalanceItem =wb.getQuantity();
                    log.info("{} quantityComponentsInWarehouseBalanceItem", quantityComponentsInWarehouseBalanceItem);
                    if (componentQuantityToWriteOff.subtract(quantityComponentsInWarehouseBalanceItem).signum() >= 0) {

                        componentQuantityToWriteOff = componentQuantityToWriteOff.subtract(quantityComponentsInWarehouseBalanceItem);
                        log.info("{} componentQuantityToWriteOff after", componentQuantityToWriteOff);
                        warehouseBalanceRepo.deleteById(warehouseBalanceId);
                        log.info("Warehouse balance was deleted from base");
                    } else {
                        wb.setQuantity(quantityComponentsInWarehouseBalanceItem.subtract(componentQuantityToWriteOff));
                        log.info("{}  wb.getQuantity()", wb.getQuantity().toString());
                        warehouseBalanceRepo.save(wb);
                        break;
                    }
                }
            }
        }
    }

}
