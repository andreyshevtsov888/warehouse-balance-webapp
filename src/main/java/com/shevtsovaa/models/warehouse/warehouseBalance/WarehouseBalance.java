package com.shevtsovaa.models.warehouse.warehouseBalance;

import com.shevtsovaa.models.consignments.component.Component;
import com.shevtsovaa.models.consignments.consignment.Consignment;
import jakarta.persistence.*;
import lombok.*;

import java.math.BigDecimal;

/**
 * @author Andrey Shevtsov on 05.06.2024.
 * @project warehouse-balance-webapp
 */

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Builder
@Entity
@Table(name = "warehouse_balance")
@AllArgsConstructor
public class WarehouseBalance {

    public WarehouseBalance(BigDecimal quantity, BigDecimal price) {
        this.quantity = quantity;
        this.price = price;
    }

    @Id
    @Column(nullable = false)
    private Long warehouseBalanceId;
    @Column(nullable = false)
    private BigDecimal quantity;
    @Column(nullable = false)
    private BigDecimal price;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "component_id")
    @ToString.Exclude
    private Component component;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "consignment_id")
    @ToString.Exclude
    private Consignment consignment;

}

