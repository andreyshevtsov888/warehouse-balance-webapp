package com.shevtsovaa.models.warehouse.warehouseBalance;

import com.shevtsovaa.DTO.SumWarehouseBalanceGroupByDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Andrey Shevtsov on 05.06.2024.
 * @project warehouse-balance-webapp
 */

@Repository
public interface WarehouseBalanceRepo extends JpaRepository<WarehouseBalance, Long> {

    @Query(value = " SELECT new com.shevtsovaa.DTO.SumWarehouseBalanceGroupByDTO (comp.componentId, comp.name, SUM(wb.quantity), comp.minQuantity) " +
            "FROM WarehouseBalance wb LEFT JOIN wb.component comp GROUP BY comp.componentId, comp.name, comp.minQuantity")
    List<SumWarehouseBalanceGroupByDTO> sumWarehouseBalancesGroupByComponent();

    @Query(value = "FROM WarehouseBalance wb LEFT JOIN wb.consignment cons WHERE component.componentId = :query ORDER BY cons.date")
    List<WarehouseBalance> findWarehouseBalanceByComponent(Long query);

}

