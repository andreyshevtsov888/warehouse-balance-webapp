package com.shevtsovaa.models.warehouse.warehouseBalance;

import com.shevtsovaa.DTO.SumWarehouseBalanceGroupByDTO;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Andrey Shevtsov on 05.06.2024.
 * @project warehouse-balance-webapp
 */

@Service
public interface WarehouseBalanceService {

    WarehouseBalanceDTO findWarehouseBalanceById(Long warehouseBalanceId);
    List<WarehouseBalanceDTO> findAllWarehouseBalances();
    void saveWarehouseBalance(WarehouseBalanceDTO warehouseBalanceDTO);
    void updateWarehouseBalance(WarehouseBalanceDTO warehouseBalanceDTO);
    void deleteWarehouseBalance(Long warehouseBalanceId);
    void sendAllComponentsOnWarehouseToWarehouseBalance(Long consignmentId);
    List<SumWarehouseBalanceGroupByDTO> findAllWarehouseBalancesGroupByComponent();
    void writeOffWarehouseBalanceToSelling(Long deliveryNoteId);
}
