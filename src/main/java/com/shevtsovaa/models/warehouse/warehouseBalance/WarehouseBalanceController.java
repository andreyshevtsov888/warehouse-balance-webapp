package com.shevtsovaa.models.warehouse.warehouseBalance;

import com.shevtsovaa.DTO.SumWarehouseBalanceGroupByDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**
 * @author Andrey Shevtsov on 12.06.2024.
 * @project warehouse-balance-webapp
 */

@Controller
public class WarehouseBalanceController {

    @Autowired
    private WarehouseBalanceService warehouseBalanceService;

    @GetMapping("/warehouseBalances")
    public String listWarehouseBalance(Model model) {
        List<WarehouseBalanceDTO> warehouseBalancesList = warehouseBalanceService.findAllWarehouseBalances();
        model.addAttribute("warehouseBalancesList", warehouseBalancesList);
        return "warehouseBalance/warehouse-balance-list";
    }

    @GetMapping("/warehouseBalances/{consignmentId}/save")
    public String sendAllComponentsOnWarehouseByConsignmentToWarehouseBalance(
            @PathVariable("consignmentId") Long consignmentId) {
        warehouseBalanceService.sendAllComponentsOnWarehouseToWarehouseBalance(consignmentId);
        return "redirect:/warehouseBalances";
    }

    @GetMapping("/warehouseBalances/{deliveryNoteId}/writeOff")
    public String writeOffWarehouseBalanceToSelling(
            @PathVariable("deliveryNoteId") Long deliveryNoteId) {
        warehouseBalanceService.writeOffWarehouseBalanceToSelling(deliveryNoteId);
        return "redirect:/warehouseBalances";
    }

    @GetMapping("/warehouseBalancesByComponent")
    public String listWarehouseBalanceGroupByComponent(Model model) {
        List<SumWarehouseBalanceGroupByDTO> warehouseBalancesByComponentList = warehouseBalanceService.findAllWarehouseBalancesGroupByComponent();
        model.addAttribute("warehouseBalancesByComponentList", warehouseBalancesByComponentList);
        return "warehouseBalance/warehouse-balance-by-component-list";
    }

}
