package com.shevtsovaa.models.warehouse.warehouseBalance;

import com.shevtsovaa.models.consignments.componentOnWarehouse.ComponentOnWarehouse;
import com.shevtsovaa.models.consignments.componentOnWarehouse.ComponentOnWarehouseDTO;

/**
 * @author Andrey Shevtsov on 12.06.2024.
 * @project warehouse-balance-webapp
 */

public class WarehouseBalanceMapper {

    public static WarehouseBalance mapToWarehouseBalance(WarehouseBalanceDTO warehouseBalanceDTO) {
        return WarehouseBalance.builder()
                .warehouseBalanceId(warehouseBalanceDTO.getWarehouseBalanceId())
                .quantity(warehouseBalanceDTO.getQuantity())
                .price(warehouseBalanceDTO.getPrice())
                .consignment(warehouseBalanceDTO.getConsignment())
                .component(warehouseBalanceDTO.getComponent())
                .build();
    }

    public static WarehouseBalanceDTO mapToWarehouseBalanceDTO(WarehouseBalance warehouseBalance) {
        return WarehouseBalanceDTO.builder()
                .warehouseBalanceId(warehouseBalance.getWarehouseBalanceId())
                .quantity(warehouseBalance.getQuantity())
                .price(warehouseBalance.getPrice())
                .consignment(warehouseBalance.getConsignment())
                .component(warehouseBalance.getComponent())
                .build();
    }

    public static WarehouseBalance mapToWarehouseBalanceFromCOW(ComponentOnWarehouse componentOnWarehouse) {
        return WarehouseBalance.builder()
                .warehouseBalanceId(componentOnWarehouse.getComponentOnWarehouseId())
                .quantity(componentOnWarehouse.getQuantity())
                .price(componentOnWarehouse.getPrice())
                .consignment(componentOnWarehouse.getConsignment())
                .component(componentOnWarehouse.getComponent())
                .build();
    }


}
