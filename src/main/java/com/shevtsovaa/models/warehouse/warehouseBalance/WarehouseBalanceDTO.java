package com.shevtsovaa.models.warehouse.warehouseBalance;

import com.shevtsovaa.models.consignments.component.Component;
import com.shevtsovaa.models.consignments.consignment.Consignment;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author Andrey Shevtsov on 05.06.2024.
 * @project warehouse-balance-webapp
 */

@Data
@Builder
public class WarehouseBalanceDTO {



    private Long warehouseBalanceId;
    private BigDecimal quantity;
    private BigDecimal price;
    private Component component;
    private Consignment consignment;

}
