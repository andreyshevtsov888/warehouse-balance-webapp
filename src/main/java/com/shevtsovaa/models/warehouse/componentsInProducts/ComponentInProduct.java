package com.shevtsovaa.models.warehouse.componentsInProducts;

import com.shevtsovaa.models.consignments.component.Component;
import com.shevtsovaa.models.deliveryNotes.product.Product;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

/**
 * @author Andrey Shevtsov on 05.06.2024.
 * @project warehouse-balance-webapp
 */

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Builder
@Entity
@Table(name = "component_in_products")
@AllArgsConstructor
public class ComponentInProduct {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Long componentInProductId;
    @Column(nullable = false)
    private Float quantityByProduct;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "component_id")
    @ToString.Exclude
    private Component component;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_id")
    @ToString.Exclude
    private Product product;

}
