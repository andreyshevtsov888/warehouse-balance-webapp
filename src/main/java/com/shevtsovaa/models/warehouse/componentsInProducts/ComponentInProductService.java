package com.shevtsovaa.models.warehouse.componentsInProducts;

import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Andrey Shevtsov on 05.06.2024.
 * @project warehouse-balance-webapp
 */

@Service
public interface ComponentInProductService {

    ComponentInProductDTO findComponentInProductById(Long componentInProductId);
    List<ComponentInProductDTO> findAllComponentInProducts();
    List<ComponentInProductDTO> findAllComponentsInProductByProduct(Long productId);
    void saveComponentInProduct(ComponentInProductDTO componentInProductDTO);
    void updateComponentInProduct(ComponentInProductDTO componentInProductDTO);
    void deleteComponentInProduct(Long componentInProductId);
    void createComponentInProduct(Long productId, ComponentInProductDTO componentInProductDTO);


}

