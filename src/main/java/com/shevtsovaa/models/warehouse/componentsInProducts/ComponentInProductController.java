package com.shevtsovaa.models.warehouse.componentsInProducts;

import com.shevtsovaa.models.consignments.component.ComponentDTO;
import com.shevtsovaa.models.consignments.component.ComponentService;
import com.shevtsovaa.models.deliveryNotes.ProductOnSelling.ProductOnSellingDTO;
import com.shevtsovaa.models.deliveryNotes.product.ProductDTO;
import com.shevtsovaa.models.deliveryNotes.product.ProductService;

import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Andrey Shevtsov on 12.06.2024.
 * @project warehouse-balance-webapp
 */

@Controller
public class ComponentInProductController {

    private final ComponentInProductService componentInProductService;
    private final ComponentService componentService;
    private final ProductService productService;

    @Autowired
    public ComponentInProductController(ComponentInProductService componentInProductService,
                                        ComponentService componentService, ProductService productService) {
        this.componentInProductService = componentInProductService;
        this.componentService = componentService;
        this.productService = productService;
    }

    @GetMapping("/componentsInProduct")
    public String listComponentInProducts(Model model) {
        List<ComponentInProductDTO> componentsInProductList = componentInProductService.findAllComponentInProducts();
        model.addAttribute("componentsInProductList", componentsInProductList);
        return "componentInProduct/component-in-product-list";
    }

    @GetMapping("componentsInProduct/{productId}/new")
    public String createComponentInProductForm(@PathVariable("productId") Long productId, Model model) {
        ComponentInProduct componentInProduct = new ComponentInProduct();
        model.addAttribute("componentInProduct", componentInProduct);
        model.addAttribute("productId", productId);

        List<ComponentDTO> componentsList = componentService.findAllComponents();
        model.addAttribute("componentsList", componentsList);
        return "componentInProduct/component-in-product-create";
    }

    @PostMapping("componentsInProduct/{productId}")
    public String createComponentInProduct(@PathVariable("productId") Long productId,
                                             @ModelAttribute("product") ComponentInProductDTO  componentInProductDTO,
                                             Model model) {
        model.addAttribute("productId", productId);
        model.addAttribute("componentInProduct", componentInProductDTO);
        componentInProductService.createComponentInProduct(productId, componentInProductDTO);
        return "redirect:/products/{productId}";
    }

    @PostMapping("componentsInProduct/save")
    public String saveComponentsInProduct(@Valid @ModelAttribute("componentInProduct") ComponentInProductDTO componentInProductDTO,
                                       BindingResult result, Model model) {
        model.addAttribute("componentInProduct", componentInProductDTO);
        componentInProductService.saveComponentInProduct(componentInProductDTO);
        return "redirect:/componentsInProduct/{componentInProductId}";
    }

    @GetMapping("componentsInProduct/{componentInProductId}/edit")
    public String editComponentInProductForm(@PathVariable("componentInProductId") Long componentInProductId, Model model) {
        ComponentInProductDTO componentInProductDTO = componentInProductService.findComponentInProductById(componentInProductId);
        model.addAttribute("componentInProduct", componentInProductDTO);

        List<ComponentDTO> componentsList = componentService.findAllComponents();
        model.addAttribute("componentsList", componentsList);
        List<ProductDTO> productsList = productService.findAllProducts();
        model.addAttribute("productsList", productsList);

        return "componentInProduct/component-in-product-edit";
    }

    @PostMapping("componentsInProduct/{componentInProductId}/edit")
    public String updateComponentInProduct(@Valid @PathVariable("componentInProductId") Long componentInProductId,
                                  @ModelAttribute("componentInProduct") ComponentInProductDTO componentInProductDTO,
                                  BindingResult result) {
        if (result.hasErrors()) {
            return "componentInProduct/component-in-product-edit";
        }
        ComponentInProductDTO componentInProductDTO1 = componentInProductService.findComponentInProductById(componentInProductId);
        componentInProductDTO.setComponentInProductId(componentInProductId);
        componentInProductDTO.setProduct(componentInProductDTO1.getProduct());
        componentInProductService.updateComponentInProduct(componentInProductDTO);
        return "redirect:/componentsInProduct";
    }

    @GetMapping("componentsInProduct/{componentInProductId}/delete")
    public String deleteComponentInProduct(@PathVariable("componentInProductId") Long componentInProductId) {
        componentInProductService.deleteComponentInProduct(componentInProductId);
        return "redirect:/componentsInProduct";
    }

}
