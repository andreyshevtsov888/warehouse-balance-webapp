package com.shevtsovaa.models.warehouse.componentsInProducts;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author Andrey Shevtsov on 05.06.2024.
 * @project warehouse-balance-webapp
 */

public interface ComponentInProductRepo extends JpaRepository<ComponentInProduct, Long> {

    @Query(value = "FROM ComponentInProduct WHERE product.productId = :query")
    List<ComponentInProduct> findComponentInProductByProduct(Long query);
}
