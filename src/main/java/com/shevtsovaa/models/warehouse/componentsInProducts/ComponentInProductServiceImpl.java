package com.shevtsovaa.models.warehouse.componentsInProducts;

import com.shevtsovaa.exceptions.ApiRequestException;
import com.shevtsovaa.exceptions.ItemNotFoundException;
import com.shevtsovaa.models.deliveryNotes.ProductOnSelling.ProductOnSelling;
import com.shevtsovaa.models.deliveryNotes.ProductOnSelling.ProductOnSellingDTO;
import com.shevtsovaa.models.deliveryNotes.product.Product;
import com.shevtsovaa.models.deliveryNotes.product.ProductRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static com.shevtsovaa.models.deliveryNotes.ProductOnSelling.ProductOnSellingMapper.mapToProductOnSelling;
import static com.shevtsovaa.models.warehouse.componentsInProducts.ComponentInProductMapper.mapToComponentInProduct;
import static com.shevtsovaa.models.warehouse.componentsInProducts.ComponentInProductMapper.mapToComponentInProductDTO;

/**
 * @author Andrey Shevtsov on 05.06.2024.
 * @project warehouse-balance-webapp
 */

@Service
@Slf4j
public class ComponentInProductServiceImpl implements ComponentInProductService {

    private final ComponentInProductRepo componentInProductRepo;
    private final ProductRepo productRepo;

    @Autowired
    public ComponentInProductServiceImpl(ComponentInProductRepo componentInProductRepo, ProductRepo productRepo) {
        this.componentInProductRepo = componentInProductRepo;
        this.productRepo = productRepo;
    }

    @Override
    public ComponentInProductDTO findComponentInProductById(Long componentInProductId) {
        ComponentInProduct componentInProduct = componentInProductRepo.findById(componentInProductId).orElseThrow(
                () -> new ApiRequestException("The component in product could not be found by ID."));
        log.info("Component in product was find by id successfully.");
        return mapToComponentInProductDTO(componentInProduct);
    }

    @Override
    public List<ComponentInProductDTO> findAllComponentInProducts() {
        List<ComponentInProduct> componentInProducts = componentInProductRepo.findAll();
        log.info("Component in product's list was gotten from base");
        return componentInProducts.stream().map(ComponentInProductMapper::mapToComponentInProductDTO).collect(Collectors.toList());
    }

    @Override
    public List<ComponentInProductDTO> findAllComponentsInProductByProduct(Long productId) {
        List<ComponentInProduct> componentsInProductByProductList = componentInProductRepo.findComponentInProductByProduct(productId);
        log.info("Component in product by product list was gotten from base");
        return componentsInProductByProductList.stream().map(ComponentInProductMapper::mapToComponentInProductDTO).collect(Collectors.toList());
    }

    @Override
    public void createComponentInProduct(Long productId, ComponentInProductDTO componentInProductDTO) {
        Product product = productRepo.findById(productId).get();
        ComponentInProduct componentInProduct = mapToComponentInProduct(componentInProductDTO);
        componentInProduct.setProduct(product);
        componentInProductRepo.save(componentInProduct);
        log.info("Component in product was create successfully.");
    }

    @Override
    public void saveComponentInProduct(ComponentInProductDTO componentInProductDTO) {
        ComponentInProduct componentInProduct = mapToComponentInProduct(componentInProductDTO);
        componentInProductRepo.save(componentInProduct);
        log.info("Component in product was saved successfully.");
    }

    @Override
    public void updateComponentInProduct(ComponentInProductDTO componentInProductDTO) {
        ComponentInProduct componentInProduct = mapToComponentInProduct(componentInProductDTO);
        log.info("Component in product was updated successfully.");
        componentInProductRepo.save(componentInProduct);
    }

    @Override
    public void deleteComponentInProduct(Long componentInProductId) {
        componentInProductRepo.findById(componentInProductId).orElseThrow(() -> new ItemNotFoundException("The component in product could not be deleted."));
        try {
            componentInProductRepo.deleteById(componentInProductId);
        } catch (RuntimeException e) {
            throw new ApiRequestException("You can't delete this item!");
        }
        log.info("Component in product was deleted successfully.");
    }

}
