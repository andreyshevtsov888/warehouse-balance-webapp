package com.shevtsovaa.models.warehouse.componentsInProducts;

import com.shevtsovaa.models.consignments.component.Component;
import com.shevtsovaa.models.deliveryNotes.product.Product;
import jakarta.validation.constraints.NotEmpty;
import lombok.Builder;
import lombok.Data;

/**
 * @author Andrey Shevtsov on 05.06.2024.
 * @project warehouse-balance-webapp
 */

@Data
@Builder
public class ComponentInProductDTO {

    private Long componentInProductId;
    @NotEmpty(message = "Quality field should not be empty.")
    private Float quantityByProduct;
    private Product product;
    private Component component;

}
