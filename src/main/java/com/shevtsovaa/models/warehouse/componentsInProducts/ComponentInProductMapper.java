package com.shevtsovaa.models.warehouse.componentsInProducts;

/**
 * @author Andrey Shevtsov on 12.06.2024.
 * @project warehouse-balance-webapp
 */

public class ComponentInProductMapper {

    public static ComponentInProduct mapToComponentInProduct(ComponentInProductDTO componentInProductDTO) {
        return ComponentInProduct.builder()
                .componentInProductId(componentInProductDTO.getComponentInProductId())
                .quantityByProduct(componentInProductDTO.getQuantityByProduct())
                .component(componentInProductDTO.getComponent())
                .product(componentInProductDTO.getProduct())
                .build();
    }


    public static ComponentInProductDTO mapToComponentInProductDTO(ComponentInProduct componentInProduct) {
        return ComponentInProductDTO.builder()
                .componentInProductId(componentInProduct.getComponentInProductId())
                .quantityByProduct(componentInProduct.getQuantityByProduct())
                .component(componentInProduct.getComponent())
                .product(componentInProduct.getProduct())
                .build();
    }

}
