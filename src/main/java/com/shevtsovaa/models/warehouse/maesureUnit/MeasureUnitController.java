package com.shevtsovaa.models.warehouse.maesureUnit;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Controller
@Slf4j
public class MeasureUnitController {

    @Autowired
    MeasureUnitService measureUnitService;

    @GetMapping("/measureUnits")
    public String listMeasureUnits(Model model) {
        List<MeasureUnitDTO> measureUnitsList = measureUnitService.findAllMeasureUnits();
        model.addAttribute("measureUnitsList", measureUnitsList);
        return "measureUnit/measure-unit-list";
    }

    @GetMapping("measureUnits/new")
    public String createMeasureUnitForm(Model model) {
        MeasureUnit measureUnit = new MeasureUnit();
        model.addAttribute("measureUnit", measureUnit);
        return "measureUnit/measure-unit-create";
    }

    @PostMapping("measureUnits/save")
    public String saveMeasureUnit(@Valid @ModelAttribute("measureUnit") MeasureUnitDTO measureUnitDTO, BindingResult result, Model model) {
        model.addAttribute("measureUnit", measureUnitDTO);
        measureUnitService.saveMeasureUnit(measureUnitDTO);
        return "redirect:/success";
    }

    @GetMapping("measureUnits/{measureUnitId}/edit")
    public String editMeasureUnitForm(@PathVariable("measureUnitId") Long measureUnitId, Model model) {
        MeasureUnitDTO measureUnitDTO = measureUnitService.findMeasureUnitById(measureUnitId);
        model.addAttribute("measureUnit", measureUnitDTO);
        return "measureUnit/measure-unit-edit";
    }

    @PostMapping("measureUnits/{measureUnitId}/edit")
    public String updateMeasureUnit(@Valid @PathVariable("measureUnitId") Long measureUnitId,
                                    @ModelAttribute("measureUnit") MeasureUnitDTO measureUnitDTO,
                                    BindingResult result) {
        measureUnitDTO.setMeasureUnitId(measureUnitId);
        measureUnitService.updateMeasureUnit(measureUnitDTO);
        return "redirect:/measureUnits";
    }

    @GetMapping("measureUnits/{measureUnitId}/delete")
    public String deleteMeasureUnit(@PathVariable("measureUnitId") Long measureUnitId) {
        measureUnitService.deleteMeasureUnit(measureUnitId);
        return "redirect:/measureUnits";
    }

    @GetMapping("measureUnits/search")
    public String searchMeasureUnits(@RequestParam(value = "query") String query, Model model) {
        List<MeasureUnitDTO> measureUnitsList = measureUnitService.searchMeasureUnits(query);
        model.addAttribute("measureUnitsList", measureUnitsList);
        return "measureUnit/measure-unit-list";
    }
}
