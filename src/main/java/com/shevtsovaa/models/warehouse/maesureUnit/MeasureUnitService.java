package com.shevtsovaa.models.warehouse.maesureUnit;

import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Service
public interface MeasureUnitService {

    MeasureUnitDTO findMeasureUnitById(Long measureUnitId);
    List<MeasureUnitDTO> findAllMeasureUnits();
    void saveMeasureUnit(MeasureUnitDTO MeasureUnitDTO);
    void updateMeasureUnit(MeasureUnitDTO measureUnitDTO);
    void deleteMeasureUnit(Long measureUnitId);
    List<MeasureUnitDTO> searchMeasureUnits(String query);
    boolean isMeasureUnitExistsInList(String name);

}
