package com.shevtsovaa.models.warehouse.maesureUnit;

import jakarta.validation.constraints.NotEmpty;
import lombok.Builder;
import lombok.Data;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Data
@Builder
public class MeasureUnitDTO {

    private long measureUnitId;
    @NotEmpty(message = "Unit name should not be empty.")
    private String name;
}
