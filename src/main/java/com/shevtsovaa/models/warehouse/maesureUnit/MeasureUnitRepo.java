package com.shevtsovaa.models.warehouse.maesureUnit;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Repository
public interface MeasureUnitRepo extends JpaRepository<MeasureUnit, Long> {

    Optional<MeasureUnit> findMeasureUnitByName(String url);

    @Query(value = "SELECT c FROM MeasureUnit c WHERE c.name LIKE CONCAT('%', :query, '%')")
    List<MeasureUnit> searchMeasureUnits(String query);
}
