package com.shevtsovaa.models.warehouse.maesureUnit;

import jakarta.persistence.*;
import lombok.*;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Builder
@Entity
@Table(name = "measure_units")
@AllArgsConstructor
public class MeasureUnit {

    public MeasureUnit(String name) {
        this.name = name;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Long measureUnitId;
    @Column(nullable = false, length = 50, unique = true)
    private String name;

}
