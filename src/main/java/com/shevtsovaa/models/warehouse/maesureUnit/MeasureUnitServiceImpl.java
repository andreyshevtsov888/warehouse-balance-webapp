package com.shevtsovaa.models.warehouse.maesureUnit;

import com.shevtsovaa.exceptions.ApiRequestException;
import com.shevtsovaa.exceptions.ItemNotFoundException;
import com.shevtsovaa.models.consignments.consignment.Consignment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static com.shevtsovaa.models.warehouse.maesureUnit.MeasureUnitMapper.mapToMeasureUnit;
import static com.shevtsovaa.models.warehouse.maesureUnit.MeasureUnitMapper.mapToMeasureUnitDTO;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Service
@Slf4j
public class MeasureUnitServiceImpl implements MeasureUnitService {

    @Autowired
    MeasureUnitRepo measureUnitRepo;

    @Override
    public MeasureUnitDTO findMeasureUnitById(Long measureUnitId) {
        MeasureUnit measureUnit = measureUnitRepo.findById(measureUnitId).orElseThrow(
                () -> new ApiRequestException("The measure unit could not be found."));
        log.info("Measure unit was find by id successfully.");
        return mapToMeasureUnitDTO(measureUnit);
    }

    @Override
    public List<MeasureUnitDTO> findAllMeasureUnits() {
        List<MeasureUnit> measureUnits = measureUnitRepo.findAll();
        if(measureUnits.isEmpty()) throw new ItemNotFoundException("Non of the measure unit was fond.");
        log.info("Measure units list was gotten from base");
        return measureUnits.stream().map(MeasureUnitMapper::mapToMeasureUnitDTO).collect(Collectors.toList());
    }

    @Override
    public void saveMeasureUnit(MeasureUnitDTO measureUnitDTO) {
        if (isMeasureUnitExistsInList(measureUnitDTO.getName())){
            log.info("Measure unit wasn't saved.");
            throw new ApiRequestException("This measure unit already exists.");}
        else {
            MeasureUnit measureUnit = mapToMeasureUnit(measureUnitDTO);
            measureUnitRepo.save(measureUnit);
            log.info("Measure unit was saved successfully.");
        }
    }
    @Override
    public void updateMeasureUnit(MeasureUnitDTO measureUnitDTO) {
        MeasureUnit measureUnit = mapToMeasureUnit(measureUnitDTO);
        log.info("Measure unit was updated successfully.");
        measureUnitRepo.save(measureUnit);
    }

    @Override
    public void deleteMeasureUnit(Long measureUnitId) {
        measureUnitRepo.findById(measureUnitId).orElseThrow(() -> new ItemNotFoundException("The measure unit could not be deleted."));
        try {
            measureUnitRepo.deleteById(measureUnitId);
        } catch (RuntimeException e) {
            throw new ApiRequestException("You can't delete this item!");
        }
        log.info("Measure unit was deleted successfully.");
    }

    @Override
    public List<MeasureUnitDTO> searchMeasureUnits(String query) {
        List<MeasureUnit> measureUnits = measureUnitRepo.searchMeasureUnits(query);
        return measureUnits.stream().map(MeasureUnitMapper::mapToMeasureUnitDTO).toList();
    }

    @Override
    public boolean isMeasureUnitExistsInList(String name) {
        return !searchMeasureUnits(name).isEmpty();
    }
}
