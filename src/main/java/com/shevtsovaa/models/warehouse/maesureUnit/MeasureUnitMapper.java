package com.shevtsovaa.models.warehouse.maesureUnit;

/**
 * @author Andrey Shevtsov on 07.06.2024.
 * @project warehouse-balance-webapp
 */

public class MeasureUnitMapper {

    public static MeasureUnit mapToMeasureUnit(MeasureUnitDTO MeasureUnitDTO) {
        return MeasureUnit.builder()
                .measureUnitId(MeasureUnitDTO.getMeasureUnitId())
                .name(MeasureUnitDTO.getName())
                .build();
    }

    public static MeasureUnitDTO mapToMeasureUnitDTO(MeasureUnit measureUnit) {
        return MeasureUnitDTO.builder()
                .measureUnitId(measureUnit.getMeasureUnitId())
                .name(measureUnit.getName())
                .build();
    }
}
