package com.shevtsovaa.models.contractors.address;

import com.shevtsovaa.models.contractors.contractor.Contractor;
import jakarta.validation.constraints.NotEmpty;
import lombok.Builder;
import lombok.Data;

/**
 * @author Andrey Shevtsov on 13.06.2024.
 * @project warehouse-balance-webapp
 */

@Data
@Builder
public class AddressDTO {

    private Long addressId;
    private String country;
    private String city;
    private String street;
    private String building;
    private String postalCode;
    private Contractor contractor;

}
