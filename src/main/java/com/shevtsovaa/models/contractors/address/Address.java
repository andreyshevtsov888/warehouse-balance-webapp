package com.shevtsovaa.models.contractors.address;

import com.shevtsovaa.models.contractors.contractor.Contractor;
import com.shevtsovaa.models.contractors.persons.Person;
import jakarta.persistence.*;
import lombok.*;

/**
 * @author Andrey Shevtsov on 13.06.2024.
 * @project warehouse-balance-webapp
 */
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Builder
@Entity
@Table(name = "address")
@AllArgsConstructor
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "address_id", nullable = false)
    private Long addressId;
    @Column(length = 50)
    private String country;
    @Column(length = 50)
    private String city;
    @Column(length = 50)
    private String street;
    @Column(length = 20)
    private String building;
    @Column(length = 20)
    private String postalCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "contractor_id")
    @ToString.Exclude
    private Contractor contractor;

}
