package com.shevtsovaa.models.contractors.address;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author Andrey Shevtsov on 13.06.2024.
 * @project warehouse-balance-webapp
 */

public interface AddressRepo extends JpaRepository<Address, Long> {

    @Query(value = "FROM Address WHERE contractor.contractorId = :query")
    List<Address> getAddressesByContractor(Long query);

}
