package com.shevtsovaa.models.contractors.address;

import java.util.List;

/**
 * @author Andrey Shevtsov on 13.06.2024.
 * @project warehouse-balance-webapp
 */

public interface AddressService {

    AddressDTO findAddressById(Long addressId);
    List<AddressDTO> findAllAddresses();
    List<AddressDTO> findAllAddressesByContractor(Long contractorId);
    void createAddress(Long contractorId, AddressDTO addressDTO);
    void saveAddress(AddressDTO addressDTO);
    void updateAddress(AddressDTO addressDTO);
    void deleteAddress(Long addressId);

}
