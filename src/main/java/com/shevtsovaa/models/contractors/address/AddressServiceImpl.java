package com.shevtsovaa.models.contractors.address;

import com.shevtsovaa.exceptions.ApiRequestException;
import com.shevtsovaa.exceptions.ItemNotFoundException;
import com.shevtsovaa.models.consignments.componentOnWarehouse.ComponentOnWarehouseMapper;
import com.shevtsovaa.models.contractors.contractor.Contractor;
import com.shevtsovaa.models.contractors.contractor.ContractorRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static com.shevtsovaa.models.contractors.address.AddressMapper.mapToAddress;
import static com.shevtsovaa.models.contractors.address.AddressMapper.mapToAddressDTO;

/**
 * @author Andrey Shevtsov on 13.06.2024.
 * @project warehouse-balance-webapp
 */

@Service
@Slf4j
public class AddressServiceImpl implements AddressService {


    private final AddressRepo addressRepo;
    private final ContractorRepo contractorRepo;

    @Autowired
    public AddressServiceImpl(AddressRepo addressRepo, ContractorRepo contractorRepo) {
        this.addressRepo = addressRepo;
        this.contractorRepo = contractorRepo;
    }

    @Override
    public AddressDTO findAddressById(Long addressId) {
        Address address = addressRepo.findById(addressId).orElseThrow(
                () -> new ApiRequestException("The address could not be found by ID."));
        log.info("Address was find by id successfully.");
        return mapToAddressDTO(address);
    }

    @Override
    public List<AddressDTO> findAllAddresses() {
        List<Address> addresses = addressRepo.findAll();
        log.info("Address list was gotten from base");
        return addresses.stream().map(AddressMapper::mapToAddressDTO).collect(Collectors.toList());
    }

    @Override
    public List<AddressDTO> findAllAddressesByContractor(Long contractorId) {
        List<Address> addressesByContractorList = addressRepo.getAddressesByContractor(contractorId);
        log.info("Address by contractor list was gotten from base");
        return addressesByContractorList.stream().map(AddressMapper::mapToAddressDTO).collect(Collectors.toList());
    }

    @Override
    public void createAddress(Long contractorId, AddressDTO addressDTO) {
        Contractor contractor = contractorRepo.findById(contractorId).get();
        Address address = mapToAddress(addressDTO);
        address.setContractor(contractor);
        addressRepo.save(address);
    }

    @Override
    public void saveAddress(AddressDTO addressDTO) {
        Address address = mapToAddress(addressDTO);
        addressRepo.save(address);
        log.info("Address was saved successfully.");
    }

    @Override
    public void updateAddress(AddressDTO addressDTO) {
        Address address = mapToAddress(addressDTO);
        log.info("Address was updated successfully.");
        addressRepo.save(address);
    }

    @Override
    public void deleteAddress(Long addressId) {
        addressRepo.findById(addressId).orElseThrow(() -> new ItemNotFoundException("The address could not be deleted."));
        try {
            addressRepo.deleteById(addressId);
        } catch (RuntimeException e) {
            throw new ApiRequestException("You can't delete this item!");
        }
        log.info("Address was deleted successfully.");
    }

}
