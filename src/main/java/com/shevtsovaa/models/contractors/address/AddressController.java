package com.shevtsovaa.models.contractors.address;

import com.shevtsovaa.models.consignments.component.ComponentDTO;
import com.shevtsovaa.models.consignments.componentOnWarehouse.ComponentOnWarehouse;
import com.shevtsovaa.models.consignments.componentOnWarehouse.ComponentOnWarehouseDTO;
import com.shevtsovaa.models.contractors.contractor.Contractor;
import com.shevtsovaa.models.contractors.contractor.ContractorDTO;
import com.shevtsovaa.models.contractors.contractor.ContractorService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Andrey Shevtsov on 13.06.2024.
 * @project warehouse-balance-webapp
 */

@Controller
@Slf4j
public class AddressController {

    private final AddressService addressService;
    private final ContractorService contractorService;

    @Autowired
    public AddressController(AddressService addressService, ContractorService contractorService) {
        this.addressService = addressService;
        this.contractorService = contractorService;
    }

    @GetMapping("/addresses")
    public String listAddresses(Model model) {
        List<AddressDTO> addressesList = addressService.findAllAddresses();
        model.addAttribute("addressesList", addressesList);
        return "address/address-list";
    }

    @GetMapping("addresses/{contractorId}/new")
    public String createAddressForm(@PathVariable("contractorId") Long contractorId, Model model) {
        Address address = new Address();
        model.addAttribute("address", address);
        model.addAttribute("contractorId", contractorId);
        List<ContractorDTO> contractorsList = contractorService.findAllContractors();
        model.addAttribute("contractorsList", contractorsList);
        return "address/address-create";
    }

    @PostMapping("addresses/{contractorId}")
    public String createAddress(@PathVariable("contractorId") Long contractorId, @ModelAttribute("contractor") AddressDTO addressDTO, Model model) {
        model.addAttribute("contractorId", contractorId);
        model.addAttribute("address", addressDTO);
        addressService.createAddress(contractorId, addressDTO);
        return "redirect:/contractors/{contractorId}";
    }

    @GetMapping("addresses/{addressId}/edit")
    public String editAddressForm(@PathVariable("addressId") Long addressId, Model model) {
        AddressDTO addressDTO = addressService.findAddressById(addressId);
        model.addAttribute("address", addressDTO);
        List<ContractorDTO> contractorsList = contractorService.findAllContractors();
        model.addAttribute("contractorsList", contractorsList);
        return "address/address-edit";
    }

    @PostMapping("addresses/{addressId}/edit")
    public String updateAddress(@Valid @PathVariable("addressId") Long addressId,
                                @ModelAttribute("address") AddressDTO addressDTO,
                                BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute("addressDTO", addressDTO);
            return "address/address-edit";
        }
        AddressDTO addressDTO1 = addressService.findAddressById(addressId);
        addressDTO.setAddressId(addressId);

        addressDTO.setContractor(addressDTO1.getContractor());
        addressService.updateAddress(addressDTO);
        return "redirect:/addresses";
    }

    @GetMapping("addresses/{addressId}/delete")
    public String deleteAddress(@PathVariable("addressId") Long addressId) {
        addressService.deleteAddress(addressId);
        return "redirect:/addresses";
    }

}
