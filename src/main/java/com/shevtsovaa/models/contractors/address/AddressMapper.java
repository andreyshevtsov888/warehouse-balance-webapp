package com.shevtsovaa.models.contractors.address;

/**
 * @author Andrey Shevtsov on 13.06.2024.
 * @project warehouse-balance-webapp
 */

public class AddressMapper {

    public static Address mapToAddress(AddressDTO addressDTO) {
        return Address.builder()
                .addressId(addressDTO.getAddressId())
                .country(addressDTO.getCountry())
                .city(addressDTO.getCity())
                .street(addressDTO.getStreet())
                .building(addressDTO.getBuilding())
                .postalCode(addressDTO.getPostalCode())
                .contractor(addressDTO.getContractor())
                .build();
    }

    public static AddressDTO mapToAddressDTO(Address address) {
        return AddressDTO.builder()
                .addressId(address.getAddressId())
                .country(address.getCountry())
                .city(address.getCity())
                .street(address.getStreet())
                .building(address.getBuilding())
                .postalCode(address.getPostalCode())
                .contractor(address.getContractor())
                .build();
    }
}
