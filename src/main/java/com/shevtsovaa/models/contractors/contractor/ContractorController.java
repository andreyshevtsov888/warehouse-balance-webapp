package com.shevtsovaa.models.contractors.contractor;

import com.shevtsovaa.models.contractors.address.AddressDTO;
import com.shevtsovaa.models.contractors.address.AddressService;
import com.shevtsovaa.models.contractors.persons.PersonDTO;
import com.shevtsovaa.models.contractors.persons.PersonService;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Controller
@Slf4j
public class ContractorController {

    private final ContractorService contractorService;
    private final AddressService addressService;
    private final PersonService personService;

    @Autowired
    public ContractorController(ContractorService contractorService, AddressService addressService, PersonService personService) {
        this.contractorService = contractorService;
        this.addressService = addressService;
        this.personService = personService;
    }

    @GetMapping("/contractors")
    public String listContractors(Model model) {
        List<ContractorDTO> contractorsList = contractorService.findAllContractors();
        model.addAttribute("contractorsList", contractorsList);
        return "contractor/contractor-list";
    }

    @GetMapping("contractors/new")
    public String createContractorForm(Model model) {
        Contractor contractor = new Contractor();
        model.addAttribute("contractor", contractor);
        List<AddressDTO> addressesList = addressService.findAllAddresses();
        model.addAttribute(" addressesList",  addressesList);
        List<PersonDTO> personsList = personService.findAllPersons();
        model.addAttribute("personsList", personsList);
        return "contractor/contractor-create";
    }

    @PostMapping("contractors/save")
    public String saveContractor(@Valid @ModelAttribute("contractor") ContractorDTO contractorDTO, BindingResult result, Model model) {
        model.addAttribute("contractor", contractorDTO);
        contractorService.saveContractor(contractorDTO);
        return "contractor/contractor-detail";
    }

    @GetMapping("contractors/{contractorId}/edit")
    public String editContractorForm(@PathVariable("contractorId") Long contractorId, Model model) {
        ContractorDTO contractorDTO = contractorService.findContractorById(contractorId);
        model.addAttribute("contractor", contractorDTO);
        List<AddressDTO> addressesList = addressService.findAllAddresses();
        model.addAttribute(" addressesList",  addressesList);
        List<PersonDTO> personsList = personService.findAllPersons();
        model.addAttribute("personsList", personsList);
        return "contractor/contractor-edit";
    }

    @PostMapping("contractors/{contractorId}/edit")
    public String updateContractor(@Valid @PathVariable("contractorId") Long contractorId,
                                  @ModelAttribute("contractor") ContractorDTO contractorDTO,
                                  BindingResult result) {
        if (result.hasErrors()) {
            return "contractor/contractor-edit";
        }
        contractorDTO.setContractorId(contractorId);
        contractorService.updateContractor(contractorDTO);
        return "redirect:/contractors";
    }

    @GetMapping("contractors/{contractorId}/delete")
    public String deleteContractor(@PathVariable("contractorId") Long contractorId) {
        contractorService.deleteContractor(contractorId);
        return "redirect:/contractors";
    }

    @GetMapping("/contractors/{contractorId}")
    public String contractorDetail(@PathVariable("contractorId") Long contractorId, Model model) {
        ContractorDTO contractorDTO = contractorService.findContractorById(contractorId);
        model.addAttribute("contractor", contractorDTO);
        List<AddressDTO> addressesByContractorList = addressService.findAllAddressesByContractor(contractorId);
        model.addAttribute("addressesByContractorList", addressesByContractorList);
        List<PersonDTO> personsByContractorList = personService.findAllPersonsByContractor(contractorId);
        model.addAttribute("personsByContractorList", personsByContractorList);
        return "contractor/contractor-detail";
    }

    @GetMapping("contractors/search")
    public String searchContractors(@RequestParam(value = "query") String query, Model model) {
        List<ContractorDTO> contractorsList = contractorService.searchContractors(query);
        model.addAttribute("contractorsList", contractorsList);
        return "contractor/contractor-list";
    }

}
