package com.shevtsovaa.models.contractors.contractor;

import com.shevtsovaa.models.ContractorType;

import com.shevtsovaa.models.contractors.address.Address;
import com.shevtsovaa.models.contractors.persons.Person;
import jakarta.validation.constraints.NotEmpty;
import lombok.Builder;
import lombok.Data;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Data
@Builder
public class ContractorDTO {

    private Long contractorId;
    @NotEmpty(message = "Contractor name should not be empty.")
    private String name;
    private ContractorType type;
    private Address address;
    private Person person;


}
