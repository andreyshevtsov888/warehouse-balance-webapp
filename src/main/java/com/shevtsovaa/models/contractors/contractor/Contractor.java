package com.shevtsovaa.models.contractors.contractor;

import com.shevtsovaa.models.ContractorType;
import com.shevtsovaa.models.consignments.componentKind.ComponentKind;
import com.shevtsovaa.models.consignments.consignment.Consignment;
import com.shevtsovaa.models.contractors.address.Address;
import com.shevtsovaa.models.contractors.persons.Person;
import com.shevtsovaa.models.deliveryNotes.deliveryNote.DeliveryNote;
import com.shevtsovaa.models.warehouse.maesureUnit.MeasureUnit;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

/**
 * @author Andrey Shevtsov on 22.05.2024.
 * @project warehouse-balance-webapp
 */

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Builder
@Entity
@Table(name = "contractors")
@AllArgsConstructor
public class Contractor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contractor_id", nullable = false)
    private Long contractorId;
    @Column(nullable = false, length = 50)
    private String name;
    @Column(nullable = false, length = 15)
    @Enumerated(EnumType.STRING)
    private ContractorType type;

}
