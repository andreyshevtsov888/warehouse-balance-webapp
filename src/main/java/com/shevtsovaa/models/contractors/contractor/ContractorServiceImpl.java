package com.shevtsovaa.models.contractors.contractor;

import com.shevtsovaa.exceptions.ApiRequestException;
import com.shevtsovaa.exceptions.ItemNotFoundException;

import com.shevtsovaa.models.contractors.address.Address;
import com.shevtsovaa.models.contractors.persons.Person;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static com.shevtsovaa.models.contractors.contractor.ContractorMapper.mapToContractor;
import static com.shevtsovaa.models.contractors.contractor.ContractorMapper.mapToContractorDTO;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Service
@Slf4j

public class ContractorServiceImpl implements ContractorService {

    @Autowired
    ContractorRepo contractorRepo;

    @Override
    public ContractorDTO findContractorById(Long contractorId) {
        Contractor contractor = contractorRepo.findById(contractorId).orElseThrow(
                () -> new ApiRequestException("The contractor could not be found by ID."));
        log.info("Contractor was find by id successfully.");
        return mapToContractorDTO(contractor);
    }

    @Override
    public List<ContractorDTO> findAllContractors() {
        List<Contractor> contractorsList = contractorRepo.findAll();
        log.info("Contractor's list was gotten from base");
        return contractorsList.stream().map(ContractorMapper::mapToContractorDTO).collect(Collectors.toList());
    }

    @Override
    public void saveContractor(ContractorDTO contractorDTO) {
        Contractor contractor = mapToContractor(contractorDTO);
        if (isContractorExistsInList(contractorDTO.getName())) {
            log.info("Contractor wasn't saved.");
            throw new ApiRequestException("Contractor with that name already exists.");
        } else {
            contractorRepo.save(contractor);
            log.info("Contractor was saved successfully.");
        }
    }

    @Override
    public void updateContractor(ContractorDTO contractorDTO) {
        Contractor contractor = mapToContractor(contractorDTO);
        log.info("Contractor was updated successfully.");
        contractorRepo.save(contractor);
    }

    @Override
    public void deleteContractor(Long contractorId) {
        contractorRepo.findById(contractorId).orElseThrow(() -> new ItemNotFoundException("The contractor could not be deleted."));
        try {
            contractorRepo.deleteById(contractorId);
        } catch (RuntimeException e) {
            throw new ApiRequestException("You can't delete this item!");
        }
        log.info("Contractor was deleted successfully.");
    }

    @Override
    public List<ContractorDTO> searchContractors(String query) {
        List<Contractor> contractors = contractorRepo.searchContractors(query);
        return contractors.stream().map(ContractorMapper::mapToContractorDTO).toList();
    }

    @Override
    public boolean isContractorExistsInList(String name) {
        return !searchContractors(name).isEmpty();
    }
}
