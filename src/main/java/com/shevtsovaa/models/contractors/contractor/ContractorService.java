package com.shevtsovaa.models.contractors.contractor;

import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Service
public interface ContractorService {

    ContractorDTO findContractorById(Long contractorId);
  //  List<ContractorDTO> findAllContractorsWithDetails();
    List<ContractorDTO> findAllContractors();
    void saveContractor(ContractorDTO contractorDTO);
    void updateContractor(ContractorDTO contractorDTO);
    void deleteContractor(Long contractorId);
    List<ContractorDTO> searchContractors(String query);
    boolean isContractorExistsInList(String name);

}
