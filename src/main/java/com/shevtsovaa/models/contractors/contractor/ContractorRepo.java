package com.shevtsovaa.models.contractors.contractor;

import com.shevtsovaa.models.contractors.address.Address;
import com.shevtsovaa.models.contractors.persons.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Repository
public interface ContractorRepo extends JpaRepository<Contractor, Long> {

    @Query(value = "SELECT c FROM Contractor c WHERE c.name LIKE CONCAT('%', :query, '%')")
    List<Contractor> searchContractors(String query);



}
