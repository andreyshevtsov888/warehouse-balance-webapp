package com.shevtsovaa.models.contractors.contractor;

/**
 * @author Andrey Shevtsov on 12.06.2024.
 * @project warehouse-balance-webapp
 */

public class ContractorMapper {

    public static Contractor mapToContractor(ContractorDTO contractorDTO) {
        return Contractor.builder()
                .contractorId(contractorDTO.getContractorId())
                .name(contractorDTO.getName())
                .type(contractorDTO.getType())
                .build();
    }


    public static ContractorDTO mapToContractorDTO(Contractor contractor) {
        return ContractorDTO.builder()
                .contractorId(contractor.getContractorId())
                .name(contractor.getName())
                .type(contractor.getType())
                .build();
    }

    public static ContractorDTO mapToContractorWithDetailsDTO(Contractor contractor) {
        return ContractorDTO.builder()
                .contractorId(contractor.getContractorId())
                .name(contractor.getName())
                .type(contractor.getType())
                .build();
    }

}
