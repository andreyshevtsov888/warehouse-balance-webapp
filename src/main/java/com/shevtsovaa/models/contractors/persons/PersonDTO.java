package com.shevtsovaa.models.contractors.persons;

import com.shevtsovaa.models.contractors.contractor.Contractor;

import lombok.Builder;
import lombok.Data;

/**
 * @author Andrey Shevtsov on 13.06.2024.
 * @project warehouse-balance-webapp
 */

@Data
@Builder
public class PersonDTO {

    private Long personId;
    private String firstName;
    private String lastName;
    private String position;
    private String phoneNumber;
    private Contractor contractor;

}
