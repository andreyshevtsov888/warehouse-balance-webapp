package com.shevtsovaa.models.contractors.persons;

import com.shevtsovaa.models.contractors.address.Address;
import com.shevtsovaa.models.contractors.address.AddressDTO;
import com.shevtsovaa.models.contractors.address.AddressService;
import com.shevtsovaa.models.contractors.contractor.ContractorDTO;
import com.shevtsovaa.models.contractors.contractor.ContractorService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

/**
 * @author Andrey Shevtsov on 13.06.2024.
 * @project warehouse-balance-webapp
 */

@Controller
@Slf4j
public class PersonController {

    private final PersonService personService;
    private final ContractorService contractorService;

    @Autowired
    public PersonController(PersonService personService, ContractorService contractorService) {
        this.personService = personService;
        this.contractorService = contractorService;
    }

    @GetMapping("/persons")
    public String listPersons(Model model) {
        List<PersonDTO> personsList = personService.findAllPersons();
        model.addAttribute("personsList", personsList);
        return "person/person-list";
    }

    @GetMapping("persons/{contractorId}/new")
    public String createPersonForm(@PathVariable("contractorId") Long contractorId, Model model) {
        Person person = new Person();
        model.addAttribute("person", person);
        model.addAttribute("contractorId", contractorId);
        List<ContractorDTO> contractorsList = contractorService.findAllContractors();
        model.addAttribute("contractorsList", contractorsList);
        return "person/person-create";
    }

    @PostMapping("persons/{contractorId}")
    public String createPerson(@PathVariable("contractorId") Long contractorId, @ModelAttribute("contractor") PersonDTO personDTO, Model model) {
        model.addAttribute("contractorId", contractorId);
        model.addAttribute("person", personDTO);
        personService.createPerson(contractorId, personDTO);
        return "redirect:/contractors/{contractorId}";
    }

    @GetMapping("persons/{personId}/edit")
    public String editPersonForm(@PathVariable("personId") Long personId, Model model) {
        PersonDTO personDTO = personService.findPersonById(personId);
        model.addAttribute("person", personDTO);
        List<ContractorDTO> contractorsList = contractorService.findAllContractors();
        model.addAttribute("contractorsList", contractorsList);
        return "person/person-edit";
    }

    @PostMapping("persons/{personId}/edit")
    public String updatePerson(@Valid @PathVariable("personId") Long personId,
                                @ModelAttribute("person") PersonDTO personDTO,
                                BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute("personDTO", personDTO);
            return "person/person-edit";
        }
        PersonDTO personDTO1 = personService.findPersonById(personId);
        personDTO.setPersonId(personId);
        personDTO.setContractor(personDTO1.getContractor());
        personService.updatePerson(personDTO);
        return "redirect:/persons";
    }

    @GetMapping("persons/{personId}/delete")
    public String deletePerson(@PathVariable("personId") Long personId) {
        personService.deletePerson(personId);
        return "redirect:/persons";
    }

}
