package com.shevtsovaa.models.contractors.persons;

/**
 * @author Andrey Shevtsov on 13.06.2024.
 * @project warehouse-balance-webapp
 */

public class PersonMapper {

    public static Person mapToPerson(PersonDTO personDTO) {
        return Person.builder()
                .personId(personDTO.getPersonId())
                .firstName(personDTO.getFirstName())
                .lastName(personDTO.getLastName())
                .position(personDTO.getPosition())
                .phoneNumber(personDTO.getPhoneNumber())
                .contractor(personDTO.getContractor())
                .build();
    }

    public static PersonDTO mapToPersonDTO(Person person) {
        return PersonDTO.builder()
                .personId(person.getPersonId())
                .firstName(person.getFirstName())
                .lastName(person.getLastName())
                .position(person.getPosition())
                .phoneNumber(person.getPhoneNumber())
                .contractor(person.getContractor())
                .build();
    }
}
