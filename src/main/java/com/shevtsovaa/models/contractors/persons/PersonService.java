package com.shevtsovaa.models.contractors.persons;

import java.util.List;

/**
 * @author Andrey Shevtsov on 13.06.2024.
 * @project warehouse-balance-webapp
 */

public interface PersonService {

    PersonDTO findPersonById(Long personId);
    List<PersonDTO> findAllPersons();
    List<PersonDTO> findAllPersonsByContractor(Long contractorId);
    void createPerson(Long contractorId, PersonDTO personDTO);
    void savePerson(PersonDTO personDTO);
    void updatePerson(PersonDTO personDTO);
    void deletePerson(Long personId);

}

