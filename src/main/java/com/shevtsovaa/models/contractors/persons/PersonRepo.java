package com.shevtsovaa.models.contractors.persons;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author Andrey Shevtsov on 13.06.2024.
 * @project warehouse-balance-webapp
 */

public interface PersonRepo extends JpaRepository<Person, Long> {

    @Query(value = "FROM Person WHERE contractor.contractorId = :query")
    List<Person> getPersonsByContractor(Long query);

}
