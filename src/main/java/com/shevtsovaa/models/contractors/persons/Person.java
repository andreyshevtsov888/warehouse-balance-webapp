package com.shevtsovaa.models.contractors.persons;

import com.shevtsovaa.models.contractors.contractor.Contractor;
import jakarta.persistence.*;
import lombok.*;

/**
 * @author Andrey Shevtsov on 13.06.2024.
 * @project warehouse-balance-webapp
 */

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Builder
@Entity
@Table(name = "persons")
@AllArgsConstructor
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "person_id", nullable = false)
    private Long personId;
    @Column(length = 50)
    private String firstName;
    @Column(length = 50)
    private String lastName;
    @Column(length = 50)
    private String position;
    @Column(length = 50)
    private String phoneNumber;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "contractor_id")
    @ToString.Exclude
    private Contractor contractor;

}

