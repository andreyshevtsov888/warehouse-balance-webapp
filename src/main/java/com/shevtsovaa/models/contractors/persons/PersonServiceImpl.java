package com.shevtsovaa.models.contractors.persons;

import com.shevtsovaa.exceptions.ApiRequestException;
import com.shevtsovaa.exceptions.ItemNotFoundException;

import com.shevtsovaa.models.contractors.contractor.Contractor;
import com.shevtsovaa.models.contractors.contractor.ContractorRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static com.shevtsovaa.models.contractors.persons.PersonMapper.mapToPerson;
import static com.shevtsovaa.models.contractors.persons.PersonMapper.mapToPersonDTO;

/**
 * @author Andrey Shevtsov on 13.06.2024.
 * @project warehouse-balance-webapp
 */

@Service
@Slf4j
public class PersonServiceImpl implements PersonService {


    private final PersonRepo personRepo;
    private final ContractorRepo contractorRepo;

    @Autowired
    public PersonServiceImpl(PersonRepo personRepo, ContractorRepo contractorRepo) {
        this.personRepo = personRepo;
        this.contractorRepo = contractorRepo;
    }

    @Override
    public PersonDTO findPersonById(Long personId) {
        Person person = personRepo.findById(personId).orElseThrow(
                () -> new ApiRequestException("The person could not be found by ID."));
        log.info("Person was find by id successfully.");
        return mapToPersonDTO(person);
    }

    @Override
    public List<PersonDTO> findAllPersons() {
        List<Person> persons = personRepo.findAll();
        log.info("Person list was gotten from base");
        return persons.stream().map(PersonMapper::mapToPersonDTO).collect(Collectors.toList());
    }

    @Override
    public List<PersonDTO> findAllPersonsByContractor(Long contractorId) {
        List<Person> personsByConyractorList = personRepo.getPersonsByContractor(contractorId);
        log.info("Persons by contractor list was gotten from base");
        return personsByConyractorList.stream().map(PersonMapper::mapToPersonDTO).collect(Collectors.toList());
    }

    @Override
    public void createPerson(Long contractorId, PersonDTO personDTO) {
        Contractor contractor = contractorRepo.findById(contractorId).get();
        Person person = mapToPerson(personDTO);
        person.setContractor(contractor);
        personRepo.save(person);
    }

    @Override
    public void savePerson(PersonDTO personDTO) {
        Person person = mapToPerson(personDTO);
        personRepo.save(person);
        log.info("Person was saved successfully.");

    }

    @Override
    public void updatePerson(PersonDTO personDTO) {
        Person person = mapToPerson(personDTO);
        log.info("Person was updated successfully.");
        personRepo.save(person);
    }

    @Override
    public void deletePerson(Long personId) {
        personRepo.findById(personId).orElseThrow(() -> new ItemNotFoundException("The address could not be deleted."));
        try {
            personRepo.deleteById(personId);
        } catch (RuntimeException e) {
            throw new ApiRequestException("You can't delete this item!");
        }
        log.info("Person was deleted successfully.");
    }

}
