package com.shevtsovaa.security.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * @author Andrey Shevtsov on 29.05.2024.
 * @project warehouse-balance-webapp
 */

@Repository
public interface UserRepo extends JpaRepository<User, Long> {

    @Query("select u from User u where u.email = ?1")
    User getReferenceByEmail(String email);

    @Query("select u from User u where u.userName = ?1")
    User getReferenceByUserName(String userName);
}