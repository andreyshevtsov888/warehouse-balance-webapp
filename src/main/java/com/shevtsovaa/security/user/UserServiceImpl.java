package com.shevtsovaa.security.user;

import com.shevtsovaa.DTO.RegistrationDTO;
import com.shevtsovaa.security.role.Role;
import com.shevtsovaa.security.role.RoleRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;

/**
 * @author Andrey Shevtsov on 12.06.2024.
 * @project warehouse-balance-webapp
 */

@Service
@Slf4j
public class UserServiceImpl implements UserService {
    @Autowired
    private RoleRepo roleRepo;

    @Autowired
    private UserRepo userRepo;

    @Override
    public void saveUser(RegistrationDTO registrationDTO) {
        User user = new User();
        user.setUserName(registrationDTO.getUserName());
        user.setUserName(registrationDTO.getUserName());
        user.setPassword(registrationDTO.getPassword());
        Role role = roleRepo.findByName("ACCOUNTANT");
        user.setRoles(Arrays.asList(role));
        userRepo.save(user);
    }

    @Override
    public UserDTO findByUsername(String username) {
        User user = userRepo.getReferenceByUserName(username);
        log.info("User name was find successfully.");
        return UserMapper.mapToUserDTO(user);
    }

    @Override
    public UserDTO findByEmail(String email) {
        User user = userRepo.getReferenceByUserName(email);
        log.info("User email was find successfully.");
        return UserMapper.mapToUserDTO(user);
    }


//    @Override
//    public UserDTO findUserById(Long userId) {
//        User user = userRepo.findById(userId).orElseThrow(
//                () -> new ApiRequestException("The user could not be found by ID."));
//        log.info("User was find by id successfully.");
//        return UserMapper.mapToUserDTO(user);
//    }
//
//    @Override
//    public List<UserDTO> findAllUsers() {
//        List<User> users = userRepo.findAll();
//        log.info("User's list was gotten from base");
//        return users.stream().map(UserMapper::mapToUserDTO).collect(Collectors.toList());
//    }
//
//    @Override
//    public void saveUser(UserDTO userDTO) {
//        User user = UserMapper.mapToUser(userDTO);
//        userRepo.save(user);
//        log.info("User was saved successfully.");
//    }
//
//    @Override
//    public void updateUser(UserDTO userDTO) {
//        User user = UserMapper.mapToUser(userDTO);
//        log.info("User was updated successfully.");
//        userRepo.save(user);
//    }
//
//    @Override
//    public void deleteUser(Long userId) {
//        userRepo.findById(userId).orElseThrow(() -> new ItemNotFoundException("The user could not be deleted."));
//        log.info("User was deleted successfully.");
//        userRepo.deleteById(userId);
//    }

}
