package com.shevtsovaa.security.user;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import lombok.Builder;
import lombok.Data;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Data
@Builder
public class UserDTO {

    private Long userId;
    @NotEmpty(message = "First name should not be empty.")
    private String userName;
    @NotEmpty(message = "Email should not be empty.")
    @Email
    private String email;
    @NotEmpty(message = "Password should not be empty.")
    private String password;
 //   private Role role;
}
