package com.shevtsovaa.security.user;

/**
 * @author Andrey Shevtsov on 12.06.2024.
 * @project warehouse-balance-webapp
 */

public class UserMapper {

    public static User mapToUser(UserDTO userDTO) {
        return User.builder()
                .userId(userDTO.getUserId())
                .userName(userDTO.getUserName())
                .email(userDTO.getEmail())
                .password(userDTO.getPassword())
                .build();
    }

    public static UserDTO mapToUserDTO(User user) {
        return UserDTO.builder()
                .userId(user.getUserId())
                .userName(user.getUserName())                .email(user.getEmail())
                .password(user.getPassword())
                .build();
    }

}
