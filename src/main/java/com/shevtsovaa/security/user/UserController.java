package com.shevtsovaa.security.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * @author Andrey Shevtsov on 04.06.2024.
 * @project warehouse-balance-webapp
 */

@Controller
public class UserController {

    @Autowired
    private UserService userService;

//    @GetMapping("/users")
//    public String listUsers(Model model) {
//        List<UserDTO> usersList = userService.findAllUsers();
//        model.addAttribute("usersList", usersList);
//        return "user/user-list";
//    }
//
//    @GetMapping("users/new")
//    public String createUserForm(Model model) {
//        User user = new User();
//        model.addAttribute("user", user);
//        return "user/user-create";
//    }
//
//    @PostMapping("users/save")
//    public String saveUser(@Valid @ModelAttribute("user") UserDTO userDTO, BindingResult result, Model model) {
//        model.addAttribute("user", userDTO);
//        userService.saveUser(userDTO);
//        return "redirect:/success";
//    }
//
//    @GetMapping("users/{userId}/edit")
//    public String editUserForm(@PathVariable("userId") Long userId, Model model) {
//        UserDTO userDTO = userService.findUserById(userId);
//        model.addAttribute("user", userDTO);
//        return "user/user-edit";
//    }
//
//    @PostMapping("users/{userId}/edit")
//    public String updateUser(@Valid @PathVariable("userId") Long userId,
//                                  @ModelAttribute("user") UserDTO userDTO,
//                                  BindingResult result) {
//        if (result.hasErrors()) {
//            return "user/user-edit";
//        }
//        userDTO.setUserId(userId);
//        userService.updateUser(userDTO);
//        return "redirect:/users";
//    }
//
//    @GetMapping("users/{userId}/delete")
//    public String deleteUser(@PathVariable("userId") Long userId) {
//        userService.deleteUser(userId);
//        return "redirect:/users";
//    }

}
