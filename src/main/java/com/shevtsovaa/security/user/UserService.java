package com.shevtsovaa.security.user;

import com.shevtsovaa.DTO.RegistrationDTO;
import org.springframework.stereotype.Service;

/**
 * @author Andrey Shevtsov on 12.06.2024.
 * @project warehouse-balance-webapp
 */

@Service
public interface UserService {

    void saveUser(RegistrationDTO registrationDTO);
    UserDTO findByUsername(String username);
    UserDTO findByEmail(String email);

//    UserDTO findUserById(Long userId);
//    List<UserDTO> findAllUsers();
//    void saveUser(UserDTO userDTO);
//    void updateUser(UserDTO userDTO);
//    void deleteUser(Long userId);

}
