//package com.shevtsovaa.security;
//
//import com.shevtsovaa.DTO.RegistrationDTO;
//import com.shevtsovaa.security.user.UserService;
//import jakarta.validation.Valid;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.ModelAttribute;
//import org.springframework.web.bind.annotation.PostMapping;
//
///**
// * @author Andrey Shevtsov on 19.06.2024.
// * @project warehouse-balance-webapp
// */
//
//@Controller
//public class AuthRegistrationController {
//
//    @Autowired
//    private UserService userService;
//
//    @GetMapping
//    public String home(){
//        return "index";
//    }
//
//    @GetMapping("/user")
//    public String user(){
//        return "Hello, user!";
//    }
//    @GetMapping("/admin")
//    public String admin(){
//        return "Hello, admin!";
//    }
//
//
//    @GetMapping("/login")
//    public String loginPage(@ModelAttribute("user") RegistrationDTO user, Model model) {
//        model.addAttribute("user", user);
//        return "security/login";
//    }
//
//    @PostMapping("register/save")
//    public String saveUser(@Valid @ModelAttribute("user") RegistrationDTO user, Model model) {
//        model.addAttribute("user", user);
//        userService.saveUser(user);
//        return "security/login";
//    }
//
//}
