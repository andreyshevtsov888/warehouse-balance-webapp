package com.shevtsovaa.security.role;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * @author Andrey Shevtsov on 19.06.2024.
 * @project warehouse-balance-webapp
 */

@Repository
public interface RoleRepo extends JpaRepository<Role, Long>, JpaSpecificationExecutor<Role> {

    @Query("select r from Role r where r.name = ?1")
    Role findByName(String name);
}
