package com.shevtsovaa.DTO;

import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

/**
 * @author Andrey Shevtsov on 19.06.2024.
 * @project warehouse-balance-webapp
 */

@Data
public class RegistrationDTO {

    private Long registrationId;
    @NotEmpty
    private String userName;
    @NotEmpty
    private String email;
    @NotEmpty
    private String password;

}
