package com.shevtsovaa.DTO;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

import static org.aspectj.runtime.internal.Conversions.floatValue;

/**
 * @author Andrey Shevtsov on 19.06.2024.
 * @project warehouse-balance-webapp
 */

@Setter
@Getter
@NoArgsConstructor
public class SumWarehouseBalanceGroupByDTO {

    private Long componentId;
    private String name;
    private BigDecimal totalQuantity;
    private Float minQuantity;


    public SumWarehouseBalanceGroupByDTO( Long componentId, String name, BigDecimal totalQuantity, Float minQuantity) {
        this.componentId = componentId;
        this.name = name;
        this.totalQuantity = totalQuantity;
        this.minQuantity = minQuantity;

    }
    public boolean isEnough(BigDecimal total, Float min) {
        return floatValue(total) - floatValue(min) > 0;
    }
}
