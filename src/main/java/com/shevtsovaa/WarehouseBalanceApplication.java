package com.shevtsovaa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Andrey Shevtsov on 22.05.2024.
 * @project warehouse-balance-webapp
 */

    @SpringBootApplication
    public class WarehouseBalanceApplication  {
        public static void main(String[] args) {
            SpringApplication.run(WarehouseBalanceApplication.class, args);
        }
    }

