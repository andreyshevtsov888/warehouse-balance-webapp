package com.shevtsovaa.exceptions;

import java.io.Serial;

/**
 * @author Andrey Shevtsov on 09.06.2024.
 * @project warehouse-balance-webapp
 */

public class ApiRequestException extends RuntimeException {

    public ApiRequestException(String message) {

        super(message);
    }
}
