package com.shevtsovaa.exceptions.handler;

import com.shevtsovaa.exceptions.WarehouseAccessForbiddenException;
import com.shevtsovaa.exceptions.ApiException;
import com.shevtsovaa.exceptions.ApiRequestException;
import com.shevtsovaa.exceptions.ItemNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;

/**
 * @author Andrey Shevtsov on 09.06.2024.
 * @project warehouse-balance-webapp
 */

@ControllerAdvice
@Slf4j
public class ApiExceptionHandler {

    @ExceptionHandler(ApiRequestException.class)
    public ModelAndView handleApiRequestException(ApiRequestException ex) {
        log.error("Exception raised = ", ex);
        ApiException apiException = new ApiException(
                ex.getMessage(),
                HttpStatus.BAD_REQUEST.value(),
                new Date());
        ModelAndView modelAndView = new ModelAndView("exception-page");
        modelAndView.addObject("apiException", apiException);
        return modelAndView;
    }

    @ExceptionHandler(WarehouseAccessForbiddenException.class)
    public ModelAndView handleAccessForbiddenException(WarehouseAccessForbiddenException ex) {
        log.error("Exception raised = ", ex);
        ApiException apiException = new ApiException(
                ex.getMessage(),
                HttpStatus.FORBIDDEN.value(),
                new Date());
        ModelAndView modelAndView = new ModelAndView("exception-page");
        modelAndView.addObject("apiException", apiException);
        return modelAndView;
    }

    @ExceptionHandler(ItemNotFoundException.class)
    public ModelAndView handleItemNotFoundException(ItemNotFoundException ex) {
        log.error("Exception raised = ", ex);
        ApiException apiException = new ApiException(
                ex.getMessage(),
                HttpStatus.NOT_FOUND.value(),
                new Date());
        ModelAndView modelAndView = new ModelAndView("exception-page");
        modelAndView.addObject("apiException", apiException);
        return modelAndView;
    }

//    @ExceptionHandler(HttpServerErrorException.InternalServerError.class)
//    public ModelAndView handleInternalServerError(HttpServerErrorException.InternalServerError ex) {
//        log.error("Exception raised = ", ex);
//        ApiException apiException = new ApiException(
//                "Something wrong with server, please reload page late...",
//                HttpStatus.INTERNAL_SERVER_ERROR.value(),
//                new Date());
//        ModelAndView modelAndView = new ModelAndView("exception-page");
//        modelAndView.addObject("apiException", apiException);
//        return modelAndView;
//    }


}
