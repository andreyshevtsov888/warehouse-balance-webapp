package com.shevtsovaa.exceptions;

import java.io.Serial;

/**
 * @author Andrey Shevtsov on 10.06.2024.
 * @project warehouse-balance-webapp
 */

public class WarehouseAccessForbiddenException extends RuntimeException {
    @Serial
    private static final long serialVersionUID = 1L;
    public WarehouseAccessForbiddenException(String message) {
        super(message);
    }
}
