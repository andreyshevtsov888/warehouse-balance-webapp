package com.shevtsovaa.exceptions;

import lombok.Data;

import java.util.Date;

/**
 * @author Andrey Shevtsov on 09.06.2024.
 * @project warehouse-balance-webapp
 */

@Data
public class ApiException {
    private final String message;
    private final Integer statusCode;
    private final Date timestamp;

}
