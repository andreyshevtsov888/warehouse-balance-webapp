package com.shevtsovaa.otherControllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author Andrey Shevtsov on 02.06.2024.
 * @project warehouse-balance-webapp
 */

@Controller
public class MainController {

    @GetMapping("/")
    public String viewIndex() {
        return "index";
    }

    @GetMapping("/success")
    public String viewSuccess() {
        return "success-page";
    }

    @GetMapping("/exception")
    public String viewException() {
        return "exception-page";
    }

    @GetMapping("/contact-info")
    public String viewContactInfo() {return "contact-info";
    }
}
